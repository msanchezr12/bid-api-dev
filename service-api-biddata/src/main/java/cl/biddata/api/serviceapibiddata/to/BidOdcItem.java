package cl.biddata.api.serviceapibiddata.to;

public class BidOdcItem {

	
	protected String codigo; //" varchar(50) COLLATE "default" NOT NULL,
	protected Integer correlativo;// " int4 NOT NULL,
	protected Integer codigocategoria; //" int4,
	protected String categoria; //" varchar(4000) COLLATE "default",
	protected Integer codigoproducto; //" int4,
	protected String producto; //" varchar(1000) COLLATE "default",
	protected String especificacioncomprador; //" varchar(4000) COLLATE "default",
	protected String especificacionproveedor; //" varchar(4000) COLLATE "default",
	protected Long cantidad;// " float8,
	protected String unidad; //" varchar(255) COLLATE "default",
	protected String moneda; //" varchar(3) COLLATE "default",
	protected Long precioneto; //" float8,
	protected Long totalcargos; //" float8,
	protected Long totaldescuentos; //" float8,
	protected Long totalimpuestos; //" float8,
	protected Long total; //" float8

	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Integer getCorrelativo() {
		return correlativo;
	}
	public void setCorrelativo(Integer correlativo) {
		this.correlativo = correlativo;
	}
	public Integer getCodigocategoria() {
		return codigocategoria;
	}
	public void setCodigocategoria(Integer codigocategoria) {
		this.codigocategoria = codigocategoria;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public Integer getCodigoproducto() {
		return codigoproducto;
	}
	public void setCodigoproducto(Integer codigoproducto) {
		this.codigoproducto = codigoproducto;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getEspecificacioncomprador() {
		return especificacioncomprador;
	}
	public void setEspecificacioncomprador(String especificacioncomprador) {
		this.especificacioncomprador = especificacioncomprador;
	}
	public String getEspecificacionproveedor() {
		return especificacionproveedor;
	}
	public void setEspecificacionproveedor(String especificacionproveedor) {
		this.especificacionproveedor = especificacionproveedor;
	}
	public Long getCantidad() {
		return cantidad;
	}
	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}
	public String getUnidad() {
		return unidad;
	}
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public Long getPrecioneto() {
		return precioneto;
	}
	public void setPrecioneto(Long precioneto) {
		this.precioneto = precioneto;
	}
	public Long getTotalcargos() {
		return totalcargos;
	}
	public void setTotalcargos(Long totalcargos) {
		this.totalcargos = totalcargos;
	}
	public Long getTotaldescuentos() {
		return totaldescuentos;
	}
	public void setTotaldescuentos(Long totaldescuentos) {
		this.totaldescuentos = totaldescuentos;
	}
	public Long getTotalimpuestos() {
		return totalimpuestos;
	}
	public void setTotalimpuestos(Long totalimpuestos) {
		this.totalimpuestos = totalimpuestos;
	}
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	
}
