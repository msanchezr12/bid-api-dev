package cl.biddata.api.serviceapibiddata.to;

import java.util.ArrayList;
import java.util.List;

public class BidLicitacionOfertas  {
	
	private List<BidLicitacionOferta> licitacionOfertas;
	
	public BidLicitacionOfertas() {
		
	}

	public List<BidLicitacionOferta> getOfertasList() {
		return licitacionOfertas;
	}

	public void setOfertasList(List<BidLicitacionOferta> licitacionOfertas) {
		this.licitacionOfertas = new ArrayList<BidLicitacionOferta>(licitacionOfertas);
	}
	
	
	
	
}
