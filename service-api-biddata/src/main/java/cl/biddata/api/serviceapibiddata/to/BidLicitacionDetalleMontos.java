package cl.biddata.api.serviceapibiddata.to;

import java.util.ArrayList;
import java.util.List;

public class BidLicitacionDetalleMontos {
	
	protected List<BidLicitacionDetalleMonto> licitacionMontos;

	public List<BidLicitacionDetalleMonto> getMontosList() {
		return licitacionMontos;
	}

	public void setMontosList(List<BidLicitacionDetalleMonto> licitacionMontos) {
		this.licitacionMontos = new ArrayList<BidLicitacionDetalleMonto>(licitacionMontos);
	}
		
}
