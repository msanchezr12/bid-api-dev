package cl.biddata.api.serviceapibiddata.to;

public class BidOdcOrdenCompraCompleta {

	
	protected String codigo; 			//" varchar(50) COLLATE "default" NOT NULL,
	protected String nombre; 			//" varchar(4000) COLLATE "default",
	protected Integer codigoestado;			//" int4,
	protected String estado;			//" varchar(50) COLLATE "default",
	protected String codigolicitacion; 	//" varchar(50) COLLATE "default",
	protected String descripcion; 		//" varchar(4000) COLLATE "default",
	protected Integer codigotipo;			//" int4,
	protected String tipo; //" varchar(3) COLLATE "default",
	protected String tipomoneda; //" varchar(3) COLLATE "default",
	protected Integer codigoestadoproveedor; //" int4,
	protected String estadoproveedor; //" varchar(50) COLLATE "default",
	protected String tieneitems; //" varchar(3) COLLATE "default",
	protected Long promediocalificacion; //" float8,
	protected Integer cantidadevaluacion; // " int4,
	protected Long descuentos; //" float8,
	protected Long cargos; //" float8,
	protected Long totalneto; //" float8,
	protected Long porcentajeiva; //" float8,
	protected Long impuestos; //" float8,
	protected Long total; // float8,
	protected String financiamiento; //" varchar(1000) COLLATE "default",
	protected String pais; //" varchar(10) COLLATE "default",
	protected String tipodespacho; //" varchar(2) COLLATE "default",
	protected String formapago; //" varchar(3) COLLATE "default",
	protected String fechacreacion; // " timestamp(6),
	protected String fechaenvio; //" timestamp(6),
	protected String fechaaceptacion; // timestamp(6),
	protected String fechacancelacion; //" timestamp(6),
	protected String fechaultimamodificacion; //" timestamp(6),
	protected Integer codigounidad; //" int4 NOT NULL,
	protected Integer codigoorganismo; //" int4 NOT NULL,
	protected Integer codigosucursal; //" int4 NOT NULL,
	protected Integer codigoproveedor; //" int4 NOT NULL

	
	
}
