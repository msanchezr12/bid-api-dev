package cl.biddata.api.serviceapibiddata.service;

import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.PathVariable;

import cl.biddata.api.serviceapibiddata.as.BidItemsResponse;
import cl.biddata.api.serviceapibiddata.as.BidLicitacionCompletaResponse;
import cl.biddata.api.serviceapibiddata.as.BidLicitacionesBasicasResponse;
import cl.biddata.api.serviceapibiddata.as.BidProveedorRequest;
import cl.biddata.api.serviceapibiddata.as.BidProveedorResponse;
import cl.biddata.api.serviceapibiddata.as.BidProveedoresResponse;
import cl.biddata.api.serviceapibiddata.exception.BidServiceException;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionBasica;
import cl.biddata.api.serviceapibiddata.to.BidUsuarioAPI;


public interface BidLicitacionService {
	
	public BidUsuarioAPI getUsuarioApiByRut(String rut) throws BidServiceException;
	
	public void addUsuarioAPI(final BidUsuarioAPI usuario) throws BidServiceException; 

	public BidLicitacionCompletaResponse getLicitacionById(final String id) throws BidServiceException;
	
	public List<BidLicitacionBasica> getLicitacionByIdEstado(final int ap_estado) throws BidServiceException;
	
	public BidItemsResponse getItemsByIdLicitacion(final String id) throws BidServiceException;
	
	public BidLicitacionesBasicasResponse getLicitacionesByFechaAdjudicada(final Date fechas) throws BidServiceException;
	
	public BidProveedoresResponse postProveedoresByRut(final List<BidProveedorRequest> parameters) throws BidServiceException, DataAccessException;

	public BidProveedorResponse postProveedorByRut(final BidProveedorRequest parameters) throws BidServiceException, DataAccessException;
	
	public BidProveedorResponse getProveedorByRut(final String rut) throws BidServiceException, DataAccessException;

}
