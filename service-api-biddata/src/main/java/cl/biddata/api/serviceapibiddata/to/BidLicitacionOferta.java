package cl.biddata.api.serviceapibiddata.to;

public class BidLicitacionOferta {
	

	protected Integer correlativo_item_oferta; 			//" int4 NOT NULL,
	protected String codigo_licitacion; 					//" varchar(50) COLLATE "default" NOT NULL,
	protected String rut_proveedor_oferta; 				//" varchar(50) COLLATE "default" NOT NULL,
	protected Long monto_unitario_oferta;					//" float8,
	protected Long cantidad_oferta;						//" float8
	
	protected int adjudicada;
	protected String especificaciones;
	
	
	public int getAdjudicada() {
		return adjudicada;
	}
	public void setAdjudicada(int adjudicada) {
		this.adjudicada = adjudicada;
	}
	public String getEspecificaciones() {
		return especificaciones;
	}
	public void setEspecificaciones(String especificaciones) {
		this.especificaciones = especificaciones;
	}
	public Integer getCorrelativo_item_oferta() {
		return correlativo_item_oferta;
	}
	public void setCorrelativo_item_oferta(Integer correlativo_item_oferta) {
		this.correlativo_item_oferta = correlativo_item_oferta;
	}
	public String getCodigo_licitacion() {
		return codigo_licitacion;
	}
	public void setCodigo_licitacion(String codigo_licitacion) {
		this.codigo_licitacion = codigo_licitacion;
	}
	public String getRut_proveedor_oferta() {
		return rut_proveedor_oferta;
	}
	public void setRut_proveedor_oferta(String rut_proveedor_oferta) {
		this.rut_proveedor_oferta = rut_proveedor_oferta;
	}
	public Long getMonto_unitario_oferta() {
		return monto_unitario_oferta;
	}
	public void setMonto_unitario_oferta(Long monto_unitario_oferta) {
		this.monto_unitario_oferta = monto_unitario_oferta;
	}
	public Long getCantidad_oferta() {
		return cantidad_oferta;
	}
	public void setCantidad_oferta(Long cantidad_oferta) {
		this.cantidad_oferta = cantidad_oferta;
	}

}
