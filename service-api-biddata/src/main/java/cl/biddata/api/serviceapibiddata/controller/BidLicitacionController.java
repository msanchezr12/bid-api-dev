package cl.biddata.api.serviceapibiddata.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.biddata.api.serviceapibiddata.as.BidLicitacionCompletaResponse;
import cl.biddata.api.serviceapibiddata.as.BidLicitacionesBasicasResponse;
import cl.biddata.api.serviceapibiddata.as.BidProveedorResponse;
import cl.biddata.api.serviceapibiddata.exception.BidServiceException;
import cl.biddata.api.serviceapibiddata.resource.Util;
import cl.biddata.api.serviceapibiddata.service.BidLicitacionService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

@RestController
@RequestMapping(value="/biddata-api-service")
public class BidLicitacionController {

//	private static final Log log = LogFactory.getLog(BidLicitacionController.class);
	long idEvento = Util.obtenerIdEvento();
	
	@Autowired
	@Qualifier("bidLicitacionAs")
	BidLicitacionService service;
	
//	@RequestMapping(value="/{rut}", method = RequestMethod.GET, produces="application/json")
//	public BidUsuarioAPI getusuarioApiByRut(@PathVariable("rut") final String rut) throws BidServiceException {
//		long idEvento = Util.obtenerIdEvento();
//		Util.loggerInfo(idEvento, "get person from rest controller");
//		return service.getUsuarioApiByRut(rut);
//	}	
		
//	@RequestMapping(value="/add", method = RequestMethod.POST, consumes="application/json")
//	public void addPerson(@RequestBody final BidUsuarioAPI usuario) throws BidServiceException {
//		long idEvento = Util.obtenerIdEvento();
//		Util.loggerInfo(idEvento, "adding new user from rest controller");
//		service.addUsuarioAPI(usuario);
//	}
	
//	@ApiImplicitParams({
//	    @ApiImplicitParam(name = "Authorization", value = "Authorization token", 
//	                      required = true, dataType = "string", paramType = "header") })
	@RequestMapping(value="/licitacion/{id}", method = RequestMethod.GET, produces="application/json")
	public BidLicitacionCompletaResponse getLicitacionById(@PathVariable("id") final String id) throws BidServiceException {
		Util.loggerDebug(idEvento, "get Licitacion from Rest Controller");
		return service.getLicitacionById(id);
	}

//	@RequestMapping(value="/estado/{ap_estado}", method = RequestMethod.GET, produces="application/json")
//	public List<BidLicitacionBasica> getLicitacionByIdEstado(@PathVariable("ap_estado") final int ap_estado) throws BidServiceException {
//		Util.loggerDebug(idEvento, "get Estado Licitaciones from Rest Controller");
//		return service.getLicitacionByIdEstado(ap_estado);
//	}
	
//	@RequestMapping(value="/items/{id}", method = RequestMethod.GET, produces="application/json")
//	public BidItemsResponse getItemsByIdLicitacion(@PathVariable("id") final String id) throws BidServiceException {
//		Util.loggerDebug(idEvento, "get Items by idLicitacion from Rest Controller");
//		return service.getItemsByIdLicitacion(id);
//	} 
	
//	@RequestMapping(value="/proveedores", method = RequestMethod.POST, produces="application/json")
//	public BidProveedoresResponse postProveedoresByRut(@RequestBody final List<BidProveedorRequest> parameters) throws BidServiceException {
//		Util.loggerDebug(idEvento, "post Proveedores from Rest Controller");
//		return service.postProveedoresByRut(parameters);
//	} 	
//		
//	@RequestMapping(value="/proveedor", method = RequestMethod.POST, produces="application/json")
//	public BidProveedorResponse postProveedorByRut(@RequestBody final BidProveedorRequest parameters) throws BidServiceException {
//		Util.loggerDebug(idEvento, "post Proveedores from Rest Controller");
//		return service.postProveedorByRut(parameters);
//	} 
	
//	@ApiImplicitParams({
//	    @ApiImplicitParam(name = "Authorization", value = "Authorization token", 
//	                      required = true, dataType = "string", paramType = "header") })
	@RequestMapping(value="/proveedor/{rut:.+}", method = RequestMethod.GET, produces="application/json")
	public BidProveedorResponse getProveedorByRut(@PathVariable("rut") final String rut) throws BidServiceException {
		Util.loggerDebug(idEvento, "get Proveedores from Rest Controller");
		return service.getProveedorByRut(rut);
	} 	
	
//	@ApiImplicitParams({
//	    @ApiImplicitParam(name = "Authorization", value = "Authorization token", 
//	                      required = true, dataType = "string", paramType = "header") })
	@RequestMapping(value="/licitacion/adjudicada/{fecha}", method = RequestMethod.GET, produces="application/json")
	public BidLicitacionesBasicasResponse getLicitacionesByFechaAdjudicada(			
			@PathVariable("fecha") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fecha) throws BidServiceException {
		Util.loggerDebug(idEvento, "get Licitacion Adjudicada from Rest Controller");
		return service.getLicitacionesByFechaAdjudicada(fecha);
	}
	
}
