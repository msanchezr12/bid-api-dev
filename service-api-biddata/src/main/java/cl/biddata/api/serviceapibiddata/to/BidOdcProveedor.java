package cl.biddata.api.serviceapibiddata.to;

public class BidOdcProveedor {
	
	protected Integer codigoproveedor; //" int4 NOT NULL,
	protected Integer codigosucursal; //" int4 NOT NULL,
	protected String rutsucursal; //" varchar(50) COLLATE "default",
	protected String nombreproveedor; //" varchar(255) COLLATE "default",
	protected String nombresucursal; //" varchar(255) COLLATE "default",
	protected String actividad; //" varchar(4000) COLLATE "default",
	protected String direccion; //" varchar(500) COLLATE "default",
	protected String comuna; //" varchar(255) COLLATE "default",
	protected String region; //" varchar(255) COLLATE "default",
	protected String pais; //" varchar(50) COLLATE "default",
	protected String nombrecontacto; //" varchar(255) COLLATE "default",
	protected String cargocontacto;//" varchar(255) COLLATE "default",
	protected String fonocontacto;//" varchar(255) COLLATE "default",
	protected String mailcontacto;//" varchar(255) COLLATE "default"

	public Integer getCodigoproveedor() {
		return codigoproveedor;
	}
	public void setCodigoproveedor(Integer codigoproveedor) {
		this.codigoproveedor = codigoproveedor;
	}
	public Integer getCodigosucursal() {
		return codigosucursal;
	}
	public void setCodigosucursal(Integer codigosucursal) {
		this.codigosucursal = codigosucursal;
	}
	public String getRutsucursal() {
		return rutsucursal;
	}
	public void setRutsucursal(String rutsucursal) {
		this.rutsucursal = rutsucursal;
	}
	public String getNombreproveedor() {
		return nombreproveedor;
	}
	public void setNombreproveedor(String nombreproveedor) {
		this.nombreproveedor = nombreproveedor;
	}
	public String getNombresucursal() {
		return nombresucursal;
	}
	public void setNombresucursal(String nombresucursal) {
		this.nombresucursal = nombresucursal;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getNombrecontacto() {
		return nombrecontacto;
	}
	public void setNombrecontacto(String nombrecontacto) {
		this.nombrecontacto = nombrecontacto;
	}
	public String getCargocontacto() {
		return cargocontacto;
	}
	public void setCargocontacto(String cargocontacto) {
		this.cargocontacto = cargocontacto;
	}
	public String getFonocontacto() {
		return fonocontacto;
	}
	public void setFonocontacto(String fonocontacto) {
		this.fonocontacto = fonocontacto;
	}
	public String getMailcontacto() {
		return mailcontacto;
	}
	public void setMailcontacto(String mailcontacto) {
		this.mailcontacto = mailcontacto;
	}
	
}
