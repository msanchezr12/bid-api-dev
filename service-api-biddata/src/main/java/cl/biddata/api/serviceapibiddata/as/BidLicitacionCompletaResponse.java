package cl.biddata.api.serviceapibiddata.as;

import cl.biddata.api.serviceapibiddata.to.BidLicitacionCompleta;

public class BidLicitacionCompletaResponse {

	protected BidLicitacionCompleta bidLicitacionCompleta;
	private Integer codigo_error;
	private String mensaje_error;

	public BidLicitacionCompleta getBidLicitacionCompleta() {
		return bidLicitacionCompleta;
	}
	public void setBidLicitacionCompleta(BidLicitacionCompleta bidLicitacionCompleta) {
		this.bidLicitacionCompleta = bidLicitacionCompleta;
	}
	public Integer getCodigo_error() {
		return codigo_error;
	}
	public void setCodigo_error(Integer codigo_error) {
		this.codigo_error = codigo_error;
	}
	public String getMensaje_error() {
		return mensaje_error;
	}
	public void setMensaje_error(String mensaje_error) {
		this.mensaje_error = mensaje_error;
	}
	
}
