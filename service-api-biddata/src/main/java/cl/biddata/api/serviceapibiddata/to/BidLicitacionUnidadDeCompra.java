package cl.biddata.api.serviceapibiddata.to;

public class BidLicitacionUnidadDeCompra {
	
	
	protected int codigo_unidad; // [codigo_unidad] [int] NOT NULL,
	protected int codigo_organismo; // [codigo_organismo] [int] NOT NULL,
	protected String rut_unidad; //[rut_unidad] [nvarchar](12) NULL,
	protected String razon_social_unidad; //[razon_social_unidad] [nvarchar](255) NULL,
	protected String nombre_unidad; //[nombre_unidad] [nvarchar](255) NULL,
	protected String region_unidad; //[region_unidad] [nvarchar](50) NULL,
	protected String comuna_unidad; //[comuna_unidad] [nvarchar](50) NULL,
	protected String direccion_unidad; //[direccion_unidad] [nvarchar](255) NULL,
	
	protected String nombre_organismo;
	protected String sector;
	
	
	public int getCodigo_unidad() {
		return codigo_unidad;
	}
	public void setCodigo_unidad(int codigo_unidad) {
		this.codigo_unidad = codigo_unidad;
	}
	public int getCodigo_organismo() {
		return codigo_organismo;
	}
	public void setCodigo_organismo(int codigo_organismo) {
		this.codigo_organismo = codigo_organismo;
	}
	public String getRut_unidad() {
		return rut_unidad;
	}
	public void setRut_unidad(String rut_unidad) {
		this.rut_unidad = rut_unidad;
	}
	public String getRazon_social_unidad() {
		return razon_social_unidad;
	}
	public void setRazon_social_unidad(String razon_social_unidad) {
		this.razon_social_unidad = razon_social_unidad;
	}
	public String getNombre_unidad() {
		return nombre_unidad;
	}
	public void setNombre_unidad(String nombre_unidad) {
		this.nombre_unidad = nombre_unidad;
	}
	public String getRegion_unidad() {
		return region_unidad;
	}
	public void setRegion_unidad(String region_unidad) {
		this.region_unidad = region_unidad;
	}
	public String getComuna_unidad() {
		return comuna_unidad;
	}
	public void setComuna_unidad(String comuna_unidad) {
		this.comuna_unidad = comuna_unidad;
	}
	public String getDireccion_unidad() {
		return direccion_unidad;
	}
	public void setDireccion_unidad(String direccion_unidad) {
		this.direccion_unidad = direccion_unidad;
	}
	public String getNombre_organismo() {
		return nombre_organismo;
	}
	public void setNombre_organismo(String nombre_organismo) {
		this.nombre_organismo = nombre_organismo;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
		

}
