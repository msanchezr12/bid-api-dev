package cl.biddata.api.serviceapibiddata.to;

public class BidLicitacionCriterio {

	private Integer correlativo_criterio; 	//" int4 NOT NULL,
	private String codigo_licitacion; 		//" varchar(50) COLLATE "default" NOT NULL,
	private String items_criterio;			//" varchar(500) COLLATE "default",
	private String descripcion_criterio;	//" varchar(4000) COLLATE "default",
	private Integer ponderacion_criterio;	//" numeric
	
	public Integer getCorrelativo_criterio() {
		return correlativo_criterio;
	}
	public void setCorrelativo_criterio(Integer correlativo_criterio) {
		this.correlativo_criterio = correlativo_criterio;
	}
	public String getCodigo_licitacion() {
		return codigo_licitacion;
	}
	public void setCodigo_licitacion(String codigo_licitacion) {
		this.codigo_licitacion = codigo_licitacion;
	}
	public String getItems_criterio() {
		return items_criterio;
	}
	public void setItems_criterio(String items_criterio) {
		this.items_criterio = items_criterio;
	}
	public String getDescripcion_criterio() {
		return descripcion_criterio;
	}
	public void setDescripcion_criterio(String descripcion_criterio) {
		this.descripcion_criterio = descripcion_criterio;
	}
	public Integer getPonderacion_criterio() {
		return ponderacion_criterio;
	}
	public void setPonderacion_criterio(Integer ponderacion_criterio) {
		this.ponderacion_criterio = ponderacion_criterio;
	}
}
