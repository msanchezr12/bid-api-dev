package cl.biddata.api.serviceapibiddata.to;

public class BidOdcComprador {

	
	protected Integer codigoorganismo; //" int4 NOT NULL,
	protected Integer codigounidad; //" int4 NOT NULL,
	protected String rutunidad; //" varchar(50) COLLATE "default",
	protected String nombreorganismo; //" varchar(255) COLLATE "default",
	protected String nombreunidad; //" varchar(255) COLLATE "default",
	protected String actividad; //" varchar(4000) COLLATE "default",
	protected String direccionunidad; //" varchar(500) COLLATE "default",
	protected String comunaunidad; //" varchar(255) COLLATE "default",
	protected String regionunidad; //" varchar(255) COLLATE "default",
	protected String pais;//" varchar(50) COLLATE "default",
	protected String nombrecontacto;//" varchar(255) COLLATE "default",
	protected String cargocontacto;//" varchar(255) COLLATE "default",
	protected String fonocontacto;//" varchar(255) COLLATE "default",
	protected String mailcontacto;//" varchar(255) COLLATE "default"
	public Integer getCodigoorganismo() {
		return codigoorganismo;
	}
	public void setCodigoorganismo(Integer codigoorganismo) {
		this.codigoorganismo = codigoorganismo;
	}
	public Integer getCodigounidad() {
		return codigounidad;
	}
	public void setCodigounidad(Integer codigounidad) {
		this.codigounidad = codigounidad;
	}
	public String getRutunidad() {
		return rutunidad;
	}
	public void setRutunidad(String rutunidad) {
		this.rutunidad = rutunidad;
	}
	public String getNombreorganismo() {
		return nombreorganismo;
	}
	public void setNombreorganismo(String nombreorganismo) {
		this.nombreorganismo = nombreorganismo;
	}
	public String getNombreunidad() {
		return nombreunidad;
	}
	public void setNombreunidad(String nombreunidad) {
		this.nombreunidad = nombreunidad;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public String getDireccionunidad() {
		return direccionunidad;
	}
	public void setDireccionunidad(String direccionunidad) {
		this.direccionunidad = direccionunidad;
	}
	public String getComunaunidad() {
		return comunaunidad;
	}
	public void setComunaunidad(String comunaunidad) {
		this.comunaunidad = comunaunidad;
	}
	public String getRegionunidad() {
		return regionunidad;
	}
	public void setRegionunidad(String regionunidad) {
		this.regionunidad = regionunidad;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getNombrecontacto() {
		return nombrecontacto;
	}
	public void setNombrecontacto(String nombrecontacto) {
		this.nombrecontacto = nombrecontacto;
	}
	public String getCargocontacto() {
		return cargocontacto;
	}
	public void setCargocontacto(String cargocontacto) {
		this.cargocontacto = cargocontacto;
	}
	public String getFonocontacto() {
		return fonocontacto;
	}
	public void setFonocontacto(String fonocontacto) {
		this.fonocontacto = fonocontacto;
	}
	public String getMailcontacto() {
		return mailcontacto;
	}
	public void setMailcontacto(String mailcontacto) {
		this.mailcontacto = mailcontacto;
	}

	
}
