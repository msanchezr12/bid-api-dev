package cl.biddata.api.serviceapibiddata.to;

import java.util.ArrayList;
import java.util.List;

public class BidProveedores {

	
	private List<BidProveedor> bidProveedores;
	
	public BidProveedores() {
		
	}

	public List<BidProveedor> getBidProveedoresList() {
		return bidProveedores;
	}

	public void setBidProveedoresList(List<BidProveedor> bidProveedores) {
		this.bidProveedores = new ArrayList<BidProveedor>(bidProveedores);
	}
	
	
	
}
