package cl.biddata.api.serviceapibiddata.to;

import java.util.ArrayList;
import java.util.List;

public class BidLicitacionCriterios {

	protected List<BidLicitacionCriterio> licitacionCriterios;

	public List<BidLicitacionCriterio> getCriteriosList() {
		return licitacionCriterios;
	}

	public void setCriteriosList(List<BidLicitacionCriterio> licitacionCriterios) {
		this.licitacionCriterios = new ArrayList<BidLicitacionCriterio>(licitacionCriterios);
	}
		
}
