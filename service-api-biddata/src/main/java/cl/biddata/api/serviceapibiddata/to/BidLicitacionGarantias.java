package cl.biddata.api.serviceapibiddata.to;

import java.util.ArrayList;
import java.util.List;

public class BidLicitacionGarantias {

	protected List<BidLicitacionGarantia> licitacionGarantias;
	
	public BidLicitacionGarantias() {

	}

	public List<BidLicitacionGarantia> getGarantiaList() {
		return licitacionGarantias;
	}

	public void setGarantiaList(List<BidLicitacionGarantia> licitacionGarantia) {
		this.licitacionGarantias = new ArrayList<BidLicitacionGarantia>(licitacionGarantia);
	}
	
	
}
