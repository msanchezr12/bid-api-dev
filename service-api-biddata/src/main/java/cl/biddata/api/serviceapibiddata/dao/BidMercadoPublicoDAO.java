package cl.biddata.api.serviceapibiddata.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.stereotype.Repository;

import cl.biddata.api.serviceapibiddata.as.BidLicitacionOfertaRequest;
import cl.biddata.api.serviceapibiddata.as.BidProveedorRequest;
import cl.biddata.api.serviceapibiddata.exception.BidBussinesException;
import cl.biddata.api.serviceapibiddata.resource.Util;
import cl.biddata.api.serviceapibiddata.to.BidLicitacion;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionBasica;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionCriterio;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionDetalleMonto;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionGarantia;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionItem;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionOferta;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionUnidadDeCompra;
import cl.biddata.api.serviceapibiddata.to.BidProveedor;
import cl.biddata.api.serviceapibiddata.to.BidUsuarioAPI;


@Repository
public class BidMercadoPublicoDAO {
	
	private static final Log log = LogFactory.getLog(BidMercadoPublicoDAO.class);

	@Autowired
	private SimpleJdbcTemplate bidSimpleJdbcTemplate;
	
	@Autowired
	private QueryProvider provider;
	
	
	public void addUsuarioAPI(final BidUsuarioAPI person, long idEvento) throws BidBussinesException {
		try {
			bidSimpleJdbcTemplate.update(provider.getQuery("addUsuario"), new Object [] {person.getRut(), person.getUsername(),person.getPassword(), person.getNombre(), person.getApellido_paterno(), person.getApellido_materno() });
		} catch (Exception e) {
			Util.loggerError(idEvento, "Error dao - addPerson"+e);
			throw new BidBussinesException("Error add new person");
		}
	}
	
	public BidUsuarioAPI getUsuarioApiByRut(final String rut, long idEvento) throws BidBussinesException {
		try {
			final List<BidUsuarioAPI> list = bidSimpleJdbcTemplate.query(provider.getQuery("getPerson"), new BeanPropertyRowMapper<BidUsuarioAPI>(BidUsuarioAPI.class), new Object[]{ rut });
			return list != null && !list.isEmpty() ? list.get(0) : null;
		} catch (Exception e) {
			Util.loggerInfo(idEvento, "Error dao - getPersonByRut"+e);
			throw new BidBussinesException("Error getPersonByRut ["+ rut+"]");
		}
	}
	
	public void enabledUserApi(final String rut, long idEvento) throws BidBussinesException {
		try {
			bidSimpleJdbcTemplate.update(provider.getQuery("enableUserApi"), new Object [] { rut });
		} catch (Exception e) {
			Util.loggerInfo(idEvento, "Error dao - enabledUserApi"+e);
			throw new BidBussinesException("Error enabledUserApi ["+ rut+"]");
		}
	}
	
	public void disableUserApi(final String rut, long idEvento) throws BidBussinesException {
		try {
			bidSimpleJdbcTemplate.update(provider.getQuery("disableUserApi"), new Object [] {rut });
		} catch (Exception e) {
			Util.loggerInfo(idEvento, "Error dao - disableUserApi"+e);
			throw new BidBussinesException("Error disableUserApi ["+ rut+"]");
		}		
	}
	
	public BidLicitacion getLicitacionById(final String id,long idEvento) throws BidBussinesException {
		
		BidLicitacion rsLicitacion  = new BidLicitacion();

		try {
			Util.loggerInfo(idEvento, "Inicio metodo DAO getLicitacionById: "+id);
			List<BidLicitacion> licitacion = bidSimpleJdbcTemplate.query(provider.getQuery("getLicitacionById"), new BeanPropertyRowMapper<BidLicitacion>(BidLicitacion.class), new Object[]{ id });
			if(Util.isNotNull(licitacion)&&!licitacion.isEmpty()){
			
				Util.loggerInfo(idEvento, "Metodo DAO getLicitacionById obteniendo resultados....");
				rsLicitacion.setCodigo_licitacion(Util.isNotNullString(licitacion.get(0).getCodigo_licitacion())&&!licitacion.get(0).getCodigo_licitacion().isEmpty() ? licitacion.get(0).getCodigo_licitacion() : "Sin Dato");
				rsLicitacion.setCantidad_reclamos(licitacion.get(0).getCantidad_reclamos());
				rsLicitacion.setNombre_licitacion(licitacion.get(0).getNombre_licitacion());
				rsLicitacion.setCodigo_tipo_licitacion(licitacion.get(0).getCodigo_tipo_licitacion());
				rsLicitacion.setDescripcion_licitacion(licitacion.get(0).getDescripcion_licitacion());
				rsLicitacion.setCodigo_estado_publicidad_ofertas(licitacion.get(0).getCodigo_estado_publicidad_ofertas());
				rsLicitacion.setCodigo_estado_licitacion(licitacion.get(0).getCodigo_estado_licitacion());
				rsLicitacion.setMoneda(licitacion.get(0).getMoneda());
				rsLicitacion.setContrato(licitacion.get(0).getContrato());
				rsLicitacion.setObras(licitacion.get(0).getObras());
				rsLicitacion.setCantidad_reclamos(licitacion.get(0).getCantidad_reclamos());
				rsLicitacion.setCodigo_tipo_licitacion(licitacion.get(0).getCodigo_tipo_licitacion());
				rsLicitacion.setFuente_financiamiento(licitacion.get(0).getFuente_financiamiento());
				rsLicitacion.setMonto_estimado(Util.isNotNullLong(licitacion.get(0).getMonto_estimado())?licitacion.get(0).getMonto_estimado():0);
				rsLicitacion.setNombre_responsable_contrato(licitacion.get(0).getNombre_responsable_contrato());
				rsLicitacion.setEmail_responsable_contrato(licitacion.get(0).getEmail_responsable_contrato());
				rsLicitacion.setNombre_responsable_pago(licitacion.get(0).getNombre_responsable_pago());
				rsLicitacion.setFono_responsable_contrato(licitacion.get(0).getFono_responsable_contrato());
				rsLicitacion.setEs_renovable(licitacion.get(0).getEs_renovable());
				rsLicitacion.setCodigo_unidad(licitacion.get(0).getCodigo_unidad());
				rsLicitacion.setCodigo_usuario(licitacion.get(0).getCodigo_usuario());
				rsLicitacion.setFecha_creacion(licitacion.get(0).getFecha_creacion());
				rsLicitacion.setFecha_cierre(licitacion.get(0).getFecha_cierre());
				rsLicitacion.setFecha_acto_apertura_economica(licitacion.get(0).getFecha_acto_apertura_economica());
				rsLicitacion.setFecha_apertura_tecnica(licitacion.get(0).getFecha_apertura_tecnica());
				rsLicitacion.setFecha_publicacion(licitacion.get(0).getFecha_publicacion());
				rsLicitacion.setFecha_adjudicacion(licitacion.get(0).getFecha_adjudicacion());
				rsLicitacion.setFecha_estimada_adjudicacion(licitacion.get(0).getFecha_estimada_adjudicacion());			
			}else{
				Util.loggerInfo(idEvento, "Metodo DAO getLicitacionById sin resultados: ");
				rsLicitacion.setCodigo_licitacion("-1");
			}
			Util.loggerInfo(idEvento, "Fin metodo DAO getLicitacionById");
		} catch (DataAccessException ex) {
			Util.loggerError(idEvento, "Error DAO - getlicitacionById"+ ex);
			throw new BidBussinesException("Error DAO getlicitacionById ["+ id +"]"+ex);	
		} catch (Exception e) {
			Util.loggerError(idEvento, "Error DAO - getlicitacionById"+ e);
			throw new BidBussinesException("Error DAO getlicitacionById ["+ id +"]"+e);
		}
		return rsLicitacion;
	}
	
	
	public List<BidLicitacionBasica> getLicitacionByIdEstado(final int ap_estado,long idEvento) throws BidBussinesException {
		try {
			
			List<BidLicitacionBasica> rsListLicitaciones;
			BidLicitacionBasica rsLicitacion;
			List<BidLicitacionBasica> licitaciones = bidSimpleJdbcTemplate.query(provider.getQuery("getLicitacionesByEstado"), new BeanPropertyRowMapper<BidLicitacionBasica>(BidLicitacionBasica.class), new Object[]{ ap_estado });
			rsListLicitaciones = new ArrayList<BidLicitacionBasica>();
						
			if(Util.isNotNull(licitaciones)) {
				for (BidLicitacionBasica licitacion : licitaciones) {
					
					rsLicitacion = new BidLicitacionBasica();
					rsLicitacion.setCodigo_licitacion(Util.isNotNullString(licitacion.getCodigo_licitacion())&&!licitacion.getCodigo_licitacion().isEmpty() ? licitacion.getCodigo_licitacion() : "Sin Dato");
					rsLicitacion.setNombre_licitacion(licitacion.getNombre_licitacion());
					rsLicitacion.setCodigo_estado_licitacion(licitacion.getCodigo_estado_licitacion());
					rsLicitacion.setFecha_adjudicacion(licitacion.getFecha_adjudicacion());
					rsListLicitaciones.add(rsLicitacion);
				}
			}
			return rsListLicitaciones;
			
		} catch (Exception e) {
			log.error("Error dao - getLicitacionByIdEstado", e);
			throw new BidBussinesException("Error getLicitacionByIdEstado ["+ ap_estado +"]");
		}
	}
	
	
	public List<BidLicitacionItem> getItemsByIdLicitacion(final String id, long idEvento) throws BidBussinesException {
		try {
			Util.loggerInfo(idEvento, "Inicio metodo DAO getItemsByIdLicitacion");
			final List<BidLicitacionItem> list = bidSimpleJdbcTemplate.query(provider.getQuery("getItemsByIdLicitacion"),new BeanPropertyRowMapper<BidLicitacionItem>(BidLicitacionItem.class), new Object[]{ id });
			Util.loggerInfo(idEvento, "Fin metodo DAO getItemsByIdLicitacion");
			return list != null && !list.isEmpty() ? list : null;
		} catch (Exception e) {
			log.error("Error dao - getItemsByIdLicitacion", e);
			throw new BidBussinesException("Error getItemsByIdLicitacion ["+ id +"]");
		}
		
	}
	
	public List<BidLicitacionOferta> getOfertaByIdLicitacion(final BidLicitacionOfertaRequest parameters,long idEvento) throws BidBussinesException {
		try {
			Util.loggerInfo(idEvento, "Inicio metodo DAO getOfertaByIdLicitacion");
			final List<BidLicitacionOferta> ofertas = bidSimpleJdbcTemplate.query(provider.getQuery("getOfertasByIdLicitacion"),new BeanPropertyRowMapper<BidLicitacionOferta>(BidLicitacionOferta.class), new Object[]{ parameters.getCodigo_licitacion(),parameters.getCorrelativo_item() } );
			Util.loggerInfo(idEvento, "Fin metodo DAO getOfertaByIdLicitacion");
			return ofertas != null && !ofertas.isEmpty() ? ofertas : null;	
		} catch (Exception e) {
			log.error("Error dao - getItemsByIdLicitacion", e);
			throw new BidBussinesException("Error getItemsByIdLicitacion ["+ parameters +"]");
		}
		
	}
	
	
	public List<BidLicitacionBasica> getLicitacionesByFechaAdjudicada(final Date fecha1,final Date fecha2, long idEvento) throws BidBussinesException {
		try {
			Util.loggerInfo(idEvento, "Inicio DAO - getLicitacionesByFechaAdjudicada");
			final List<BidLicitacionBasica> list = bidSimpleJdbcTemplate.query(provider.getQuery("getLicitacionByFechaAdjudicada"),new BeanPropertyRowMapper<BidLicitacionBasica>(BidLicitacionBasica.class), new Object[]{ Util.convertDateToStr(fecha1),Util.convertDateToStr(fecha2) });
			Util.loggerInfo(idEvento, "Fin DAO - getLicitacionesByFechaAdjudicada");
			return list != null && !list.isEmpty() ? list : null;	
		}catch (DataAccessException ex){	
			Util.loggerError(idEvento, "Error dao - getLicitacionesByFechaAdjudicada "+ex);
			throw new BidBussinesException("Error getLicitacionesByFechaAdjudicada ["+ fecha1 + "]["+ fecha2 + "] "+ex);
		} catch (Exception e) {
			Util.loggerError(idEvento, "Error dao - getLicitacionesByFechaAdjudicada "+e);
			throw new BidBussinesException("Error getLicitacionesByFechaAdjudicada ["+ fecha1 + "]["+ fecha2 + "] "+e);
		}
	}

	
	public List<BidLicitacionGarantia> getGarantiaByIdLicitacion(final String id,long idEvento) throws BidBussinesException {
		try {
			Util.loggerInfo(idEvento, "Inicio DAO - getGarantiaByIdLicitacion");
			final List<BidLicitacionGarantia> list = bidSimpleJdbcTemplate.query(provider.getQuery("getGarantiaByIdLicitacion"),new BeanPropertyRowMapper<BidLicitacionGarantia>(BidLicitacionGarantia.class), new Object[]{ id });
			Util.loggerInfo(idEvento, "Fin DAO - getGarantiaByIdLicitacion");
			return list != null && !list.isEmpty() ? list : null;			
		} catch (Exception e) {
			log.error("Error dao - getLicitacionesByFechaAdjudicada");
			throw new BidBussinesException("Error getLicitacionesByFechaAdjudicada ["+ id +"] ");
		}
	}
	
	public List<BidLicitacionCriterio> getCriterioByIdLicitacion(final String id,long idEvento) throws BidBussinesException {
		try {
			
			Util.loggerInfo(idEvento, "Inicio metodo DAO getCriterioByIdLicitacion ");
			final List<BidLicitacionCriterio> list = bidSimpleJdbcTemplate.query(provider.getQuery("getCriterioByIdLicitacion"),new BeanPropertyRowMapper<BidLicitacionCriterio>(BidLicitacionCriterio.class), new Object[]{ id });
			Util.loggerInfo(idEvento, "Termino metodo DAO getCriterioByIdLicitacion");
			return list != null && !list.isEmpty() ? list : null;			
		} catch (Exception e) {
			Util.loggerError(idEvento, "Error dao - getCriterioByIdLicitacion"+e);
			throw new BidBussinesException("Error getCriterioByIdLicitacion ["+ id +"] ");
		}		
	}
	
	public BidProveedor postProveedor(long idEvento, final BidProveedorRequest parameters) throws BidBussinesException, DataAccessException {
	
		List<BidProveedor> list;
		try {
			list = bidSimpleJdbcTemplate.query(provider.getQuery("getProveedorByRut"),new BeanPropertyRowMapper<BidProveedor>(BidProveedor.class), new Object[]{ parameters.getRut_proveedor() });
			if(Util.isNotNull(list)) {
				log.info("Proveedor encontrado...");
			}
		}catch (DataAccessException ex){
			log.error("Error dao - getProveedorByRut");
			throw new BidBussinesException("Error getProveedorByRut ["+ parameters.getRut_proveedor() +"] "+ex);
		} catch (Exception e) {
			log.error("Error dao - getProveedorByRut");
			throw new BidBussinesException("Error getProveedorByRut ["+ parameters.getRut_proveedor() +"] "+e);
		}	
		return list != null && !list.isEmpty() ? list.get(0) : null;
	}
	
	
	public BidProveedor getProveedorByRut(long idEvento, final String rut) throws BidBussinesException, DataAccessException {
		
		List<BidProveedor> rsProveedorList;
		BidProveedor rsProveedor;
		try {
			Util.loggerDebug(idEvento, "Inicio metodo DAO getProveedorByRut: "+rut);
			rsProveedorList = bidSimpleJdbcTemplate.query(provider.getQuery("getProveedorByRut"),new BeanPropertyRowMapper<BidProveedor>(BidProveedor.class), new Object[]{ rut });
			rsProveedor = new BidProveedor();
			if(Util.isNotNull(rsProveedorList)&&!rsProveedorList.isEmpty()) {
				
				rsProveedor.setRut_proveedor(rsProveedorList.get(0).getRut_proveedor());
				rsProveedor.setRazon_social_proveedor(rsProveedorList.get(0).getRazon_social_proveedor());
				rsProveedor.setEmail_usuario_proveedor(rsProveedorList.get(0).getEmail_usuario_proveedor());
				rsProveedor.setNombre_proveedor(rsProveedorList.get(0).getNombre_proveedor());
				rsProveedor.setRegion_proveedor(rsProveedorList.get(0).getRegion_proveedor());
				rsProveedor.setComuna_proveedor(rsProveedorList.get(0).getComuna_proveedor());
				rsProveedor.setDireccion_proveedor(rsProveedorList.get(0).getDireccion_proveedor());
				rsProveedor.setId_estado_proveedor(rsProveedorList.get(0).getId_estado_proveedor());
				rsProveedor.setFono1_usuario_proveedor((rsProveedorList.get(0).getFono1_usuario_proveedor()));
				rsProveedor.setLink_proveedor(rsProveedorList.get(0).getLink_proveedor());
				rsProveedor.setCertificado_socios(rsProveedorList.get(0).getCertificado_socios());
				rsProveedor.setContacto(rsProveedorList.get(0).getContacto());
				rsProveedor.setFono_alternativo(rsProveedorList.get(0).getFono_alternativo());
				rsProveedor.setEmail_alternativo(rsProveedorList.get(0).getEmail_alternativo());
				rsProveedor.setCodigo_error(Util.COD_OK);
				//rsProveedor.setMensaje_error(Util.ESTADO_OK);
			}else {
				rsProveedor.setRut_proveedor(rut);
				rsProveedor.setCodigo_error(Util.COD_ERROR);
				rsProveedor.setMensaje_error(Util.MSG_WARNING_PROVEEDOR_NO_EXISTE);
			}
			Util.loggerDebug(idEvento, "Fin metodo DAO getProveedorByRut");
		}catch (DataAccessException ex){
			log.error("Error dao - getProveedorByRut");
			throw new BidBussinesException("Error getProveedorByRut ["+ rut +"] "+ex);
		} catch (Exception e) {
			log.error("Error dao - getProveedorByRut");
			throw new BidBussinesException("Error getProveedorByRut ["+ rut +"] "+e);
		}	
		return rsProveedor;
	}	
	
	
	/************** MONTOS LICITACION ***********************/

	public List<BidLicitacionDetalleMonto> getLicitacionDetalleMontoById(final String id,long idEvento) throws BidBussinesException {
		try {
			Util.loggerInfo(idEvento, "Inicio metodo DAO getLicitacionDetalleMontoById ");
			final List<BidLicitacionDetalleMonto> list = bidSimpleJdbcTemplate.query(provider.getQuery("getLicitacionDetalleMontoById"),new BeanPropertyRowMapper<BidLicitacionDetalleMonto>(BidLicitacionDetalleMonto.class), new Object[]{ id });
			Util.loggerInfo(idEvento, "Termino metodo DAO getLicitacionDetalleMontoById");
			return list != null && !list.isEmpty() ? list : null;			
		} catch (Exception e) {
			Util.loggerError(idEvento, "Error dao - getLicitacionDetalleMontoById"+e);
			throw new BidBussinesException("Error getLicitacionDetalleMontoById ["+ id +"] ");
		}		
	}
	

	/************** UNIDADES DE COMPRA LICITACION ***********************/
	
	public List<BidLicitacionUnidadDeCompra> getLicitacionUnidadDeCompraById(final String id,long idEvento) throws BidBussinesException {
		try {
			Util.loggerInfo(idEvento, "Inicio metodo DAO getLicitacionUnidadDeCompraById ");
			final List<BidLicitacionUnidadDeCompra> list = bidSimpleJdbcTemplate.query(provider.getQuery("getLicitacionUnidadDeCompraById"),new BeanPropertyRowMapper<BidLicitacionUnidadDeCompra>(BidLicitacionUnidadDeCompra.class), new Object[]{ id });
			Util.loggerInfo(idEvento, "Termino metodo DAO getLicitacionUnidadDeCompraById");
			return list != null && !list.isEmpty() ? list : null;			
		} catch (Exception e) {
			Util.loggerError(idEvento, "Error dao - getLicitacionUnidadDeCompraById"+e);
			throw new BidBussinesException("Error getLicitacionUnidadDeCompraById ["+ id +"] ");
		}		
	}

	
}
