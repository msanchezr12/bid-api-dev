package cl.biddata.api.serviceapibiddata.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class BidUsuarioAPI implements Serializable, UserDetails {

	
	private static final long serialVersionUID = -7323444984712689934L;	
	
	protected String rut; //" varchar(10) COLLATE "default" NOT NULL,
	protected String username; //" varchar(50) COLLATE "default",
	protected String password; // " varchar(255) COLLATE "default",
	protected boolean enabled; //" bool,
	protected String nombre; 	//" varchar(50) COLLATE "default",
	protected String apellido_materno;  //" varchar(50) COLLATE "default",
	protected String apellido_paterno; //" varchar(50) COLLATE "default"
	
	
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isEnable() {
		return enabled;
	}
	public void setEnable(boolean enable) {
		this.enabled = enable;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido_materno() {
		return apellido_materno;
	}
	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}
	public String getApellido_paterno() {
		return apellido_paterno;
	}
	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}
	
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> authoritys = new ArrayList<GrantedAuthority>();
		authoritys.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		return authoritys;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	
}
