package cl.biddata.api.serviceapibiddata.to;

import java.util.ArrayList;
import java.util.List;

public class BidLicitacionItems {
	
	private List<BidLicitacionItem> licitacionItems;

	public BidLicitacionItems() {
	}
	
	public List<BidLicitacionItem> getItemsList() {
		return licitacionItems;
	}

	public void setItemsList(List<BidLicitacionItem> licitacionItems) {
		this.licitacionItems = new ArrayList<BidLicitacionItem>(licitacionItems);
	}
	
	
	
	
}
