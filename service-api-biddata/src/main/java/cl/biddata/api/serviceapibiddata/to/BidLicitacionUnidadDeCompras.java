package cl.biddata.api.serviceapibiddata.to;

import java.util.ArrayList;
import java.util.List;

public class BidLicitacionUnidadDeCompras {
	
	protected List<BidLicitacionUnidadDeCompra> licitacionUnidadDeCompras;

	public List<BidLicitacionUnidadDeCompra> getUnidadDeComprasList() {
		return licitacionUnidadDeCompras;
	}

	public void setUnidadDeComprasList(List<BidLicitacionUnidadDeCompra> licitacionUnidadDeCompras) {
		this.licitacionUnidadDeCompras = new ArrayList<BidLicitacionUnidadDeCompra>(licitacionUnidadDeCompras);
	}

}
