package cl.biddata.api.serviceapibiddata.to;

public class BidLicitacionItem {
	
	protected Integer correlativo_item; 			//" int4 NOT NULL,
	protected Integer codigo_producto_item; 		//" int4,
	protected String codigo_licitacion; 		//varchar(50) COLLATE "default" NOT NULL,
	protected String descripcion_item; 		//varchar(1000) COLLATE "default",
	protected Long cantidad_item;				//" float8

	protected String nombre_producto;
	protected String unidad_producto;
	protected String codigo_categoria; 
	protected String nombre_categoria;
	
	public String getNombre_producto() {
		return nombre_producto;
	}
	public void setNombre_producto(String nombre_producto) {
		this.nombre_producto = nombre_producto;
	}
	public String getUnidad_producto() {
		return unidad_producto;
	}
	public void setUnidad_producto(String unidad_producto) {
		this.unidad_producto = unidad_producto;
	}
	public String getCodigo_categoria() {
		return codigo_categoria;
	}
	public void setCodigo_categoria(String codigo_categoria) {
		this.codigo_categoria = codigo_categoria;
	}
	public String getNombre_categoria() {
		return nombre_categoria;
	}
	public void setNombre_categoria(String nombre_categoria) {
		this.nombre_categoria = nombre_categoria;
	}
	private BidLicitacionOfertas itemOferta;
	
	public BidLicitacionOfertas getItemOferta() {
		return itemOferta;
	}
	public void setItemOferta(BidLicitacionOfertas itemOferta) {
		this.itemOferta = itemOferta;
	}
	public Integer getCorrelativo_item() {
		return correlativo_item;
	}
	public void setCorrelativo_item(Integer correlativo_item) {
		this.correlativo_item = correlativo_item;
	}
	public Integer getCodigo_producto_item() {
		return codigo_producto_item;
	}
	public void setCodigo_producto_item(Integer codigo_producto_item) {
		this.codigo_producto_item = codigo_producto_item;
	}
	public String getCodigo_licitacion() {
		return codigo_licitacion;
	}
	public void setCodigo_licitacion(String codigo_licitacion) {
		this.codigo_licitacion = codigo_licitacion;
	}
	public String getDescripcion_item() {
		return descripcion_item;
	}
	public void setDescripcion_item(String descripcion_item) {
		this.descripcion_item = descripcion_item;
	}
	public Long getCantidad_item() {
		return cantidad_item;
	}
	public void setCantidad_item(Long cantidad_item) {
		this.cantidad_item = cantidad_item;
	}
	
}
