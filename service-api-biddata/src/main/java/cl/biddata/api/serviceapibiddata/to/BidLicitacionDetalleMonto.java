package cl.biddata.api.serviceapibiddata.to;

public class BidLicitacionDetalleMonto {
	
	protected String codigo_licitacion;
//	protected double monto_estimado_contrato;
//	protected double monto_neto_adjudicado;
	protected String monto_estimado_contrato;
	protected String monto_neto_adjudicado;
		
//	public double getMonto_estimado_contrato() {
//		return monto_estimado_contrato;
//	}
//	public void setMonto_estimado_contrato(double monto_estimado_contrato) {
//		this.monto_estimado_contrato = monto_estimado_contrato;
//	}
//	public double getMonto_neto_adjudicado() {
//		return monto_neto_adjudicado;
//	}
//	public void setMonto_neto_adjudicado(double monto_neto_adjudicado) {
//		this.monto_neto_adjudicado = monto_neto_adjudicado;
//	}
	
	public String getCodigo_licitacion() {
		return codigo_licitacion;
	}
	public void setCodigo_licitacion(String codigo_licitacion) {
		this.codigo_licitacion = codigo_licitacion;
	}
	
	public String getMonto_estimado_contrato() {
		return monto_estimado_contrato;
	}
	public void setMonto_estimado_contrato(String monto_estimado_contrato) {
		this.monto_estimado_contrato = monto_estimado_contrato;
	}
	public String getMonto_neto_adjudicado() {
		return monto_neto_adjudicado;
	}
	public void setMonto_neto_adjudicado(String monto_neto_adjudicado) {
		this.monto_neto_adjudicado = monto_neto_adjudicado;
	}
	
	

}
