package cl.biddata.api.serviceapibiddata.to;

import java.util.Date;

public class BidLicitacionBasica {

	protected String codigo_licitacion; 				//  Y    varchar(50) COLLATE "default" NOT NULL,
	protected String nombre_licitacion; 				//  Y varchar(500) COLLATE "default",
	protected Integer codigo_estado_licitacion; 			//  Y  int4,
	protected String fecha_adjudicacion; 				//  Y  timestamp(6);
	
	public String getCodigo_licitacion() {
		return codigo_licitacion;
	}
	public void setCodigo_licitacion(String codigo_licitacion) {
		this.codigo_licitacion = codigo_licitacion;
	}
	public String getNombre_licitacion() {
		return nombre_licitacion;
	}
	public void setNombre_licitacion(String nombre_licitacion) {
		this.nombre_licitacion = nombre_licitacion;
	}
	
	public Integer getCodigo_estado_licitacion() {
		return codigo_estado_licitacion;
	}
	public void setCodigo_estado_licitacion(Integer codigo_estado_licitacion) {
		this.codigo_estado_licitacion = codigo_estado_licitacion;
	}	
	public String getFecha_adjudicacion() {
		return fecha_adjudicacion;
	}
	public void setFecha_adjudicacion(String fecha_adjudicacion) {
		this.fecha_adjudicacion = fecha_adjudicacion;
	}
	
	
}
