package cl.biddata.api.serviceapibiddata.to;

import java.util.Date;

public class BidLicitacionCompleta {
	
	protected String codigo_licitacion; 					//  varchar(50) COLLATE "default" NOT NULL,
	protected String nombre_licitacion; 					// varchar(500) COLLATE "default",
	protected Integer codigo_tipo_licitacion; 			// int4,
	protected String descripcion_licitacion;   			// varchar(4000) COLLATE "default",
	protected Integer codigo_estado_publicidad_ofertas; 	// int4,
	protected Integer codigo_estado_licitacion; 			// int4,
	protected String moneda; 								// varchar(50) COLLATE "default",
	protected String contrato; 							// varchar(50) COLLATE "default",
	protected String obras;  								//varchar(50) COLLATE "default",
	protected Integer cantidad_reclamos;					//int4,
	protected Integer codigo_tipo_pago_licitacion;		// int4,
	protected String fuente_financiamiento;				// varchar(500) COLATE "default",
	protected Long  monto_estimado; 						// float8,
	protected String nombre_responsable_pago; 			// varchar(250) COLLATE "default",
	protected String email_responsable_pago; 				//varchar(250) COLLATE "default",
	protected String nombre_responsable_contrato; 		//varchar(250) COLLATE "default",
	protected String email_responsable_contrato; 			//varchar(250) COLLATE "default",
	protected String fono_responsable_contrato;			// varchar(250) COLLATE "default",
	protected Integer es_renovable; 						//int4,
	protected Integer codigo_unidad; 						//int4,
	protected Integer codigo_usuario;						// int4,
	protected String fecha_creacion;						// timestamp(6),
	protected String fecha_cierre; 							//timestamp(6),
	protected String fecha_apertura_tecnica;				// timestamp(6),
	protected String fecha_acto_apertura_economica; 		//timestamp(6),
	protected String fecha_publicacion; 					//timestamp(6),
	protected String fecha_adjudicacion; 					//timestamp(6);
	protected String fecha_estimada_adjudicacion; 			//timestamp(6);

	
	protected BidLicitacionItems licitacionItems;
	protected BidLicitacionGarantias licitacionGarantias;
	protected BidLicitacionCriterios licitacionCriterios;
	
	protected BidLicitacionDetalleMontos licitacionMontos;
	protected BidLicitacionUnidadDeCompras licitacionUnidadCompras;
	
	
	
			
	public BidLicitacionUnidadDeCompras getLicitacionUnidadCompras() {
		return licitacionUnidadCompras;
	}
	public void setLicitacionUnidadCompras(BidLicitacionUnidadDeCompras licitacionUnidadCompras) {
		this.licitacionUnidadCompras = licitacionUnidadCompras;
	}
	public BidLicitacionDetalleMontos getLicitacionMontos() {
		return licitacionMontos;
	}
	public void setLicitacionMontos(BidLicitacionDetalleMontos licitacionMontos) {
		this.licitacionMontos = licitacionMontos;
	}
	public BidLicitacionGarantias getLicitacionGarantias() {
		return licitacionGarantias;
	}
	public void setLicitacionGarantias(BidLicitacionGarantias licitacionGarantias) {
		this.licitacionGarantias = licitacionGarantias;
	}
	public BidLicitacionCriterios getLicitacionCriterios() {
		return licitacionCriterios;
	}
	public void setLicitacionCriterios(BidLicitacionCriterios licitacionCriterios) {
		this.licitacionCriterios = licitacionCriterios;
	}
	public BidLicitacionItems getLicitacionItems() {
		return licitacionItems;
	}
	public void setLicitacionItems(BidLicitacionItems licitacionItems) {
		this.licitacionItems = licitacionItems;
	}
	
	public String getCodigo_licitacion() {
		return codigo_licitacion;
	}
	public void setCodigo_licitacion(String codigo_licitacion) {
		this.codigo_licitacion = codigo_licitacion;
	}
	public String getNombre_licitacion() {
		return nombre_licitacion;
	}
	public void setNombre_licitacion(String nombre_licitacion) {
		this.nombre_licitacion = nombre_licitacion;
	}
	public Integer getCodigo_tipo_licitacion() {
		return codigo_tipo_licitacion;
	}
	public void setCodigo_tipo_licitacion(Integer codigo_tipo_licitacion) {
		this.codigo_tipo_licitacion = codigo_tipo_licitacion;
	}
	public String getDescripcion_licitacion() {
		return descripcion_licitacion;
	}
	public void setDescripcion_licitacion(String descripcion_licitacion) {
		this.descripcion_licitacion = descripcion_licitacion;
	}
	public Integer getCodigo_estado_publicidad_ofertas() {
		return codigo_estado_publicidad_ofertas;
	}
	public void setCodigo_estado_publicidad_ofertas(Integer codigo_estado_publicidad_ofertas) {
		this.codigo_estado_publicidad_ofertas = codigo_estado_publicidad_ofertas;
	}
	public Integer getCodigo_estado_licitacion() {
		return codigo_estado_licitacion;
	}
	public void setCodigo_estado_licitacion(Integer codigo_estado_licitacion) {
		this.codigo_estado_licitacion = codigo_estado_licitacion;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public String getObras() {
		return obras;
	}
	public void setObras(String obras) {
		this.obras = obras;
	}
	public Integer getCantidad_reclamos() {
		return cantidad_reclamos;
	}
	public void setCantidad_reclamos(Integer cantidad_reclamos) {
		this.cantidad_reclamos = cantidad_reclamos;
	}
	public Integer getCodigo_tipo_pago_licitacion() {
		return codigo_tipo_pago_licitacion;
	}
	public void setCodigo_tipo_pago_licitacion(Integer codigo_tipo_pago_licitacion) {
		this.codigo_tipo_pago_licitacion = codigo_tipo_pago_licitacion;
	}
	public String getFuente_financiamiento() {
		return fuente_financiamiento;
	}
	public void setFuente_financiamiento(String fuente_financiamiento) {
		this.fuente_financiamiento = fuente_financiamiento;
	}
	public Long getMonto_estimado() {
		return monto_estimado;
	}
	public void setMonto_estimado(Long monto_estimado) {
		this.monto_estimado = monto_estimado;
	}
	public String getNombre_responsable_pago() {
		return nombre_responsable_pago;
	}
	public void setNombre_responsable_pago(String nombre_responsable_pago) {
		this.nombre_responsable_pago = nombre_responsable_pago;
	}
	public String getEmail_responsable_pago() {
		return email_responsable_pago;
	}
	public void setEmail_responsable_pago(String email_responsable_pago) {
		this.email_responsable_pago = email_responsable_pago;
	}
	public String getNombre_responsable_contrato() {
		return nombre_responsable_contrato;
	}
	public void setNombre_responsable_contrato(String nombre_responsable_contrato) {
		this.nombre_responsable_contrato = nombre_responsable_contrato;
	}
	public String getEmail_responsable_contrato() {
		return email_responsable_contrato;
	}
	public void setEmail_responsable_contrato(String email_responsable_contrato) {
		this.email_responsable_contrato = email_responsable_contrato;
	}
	public String getFono_responsable_contrato() {
		return fono_responsable_contrato;
	}
	public void setFono_responsable_contrato(String fono_responsable_contrato) {
		this.fono_responsable_contrato = fono_responsable_contrato;
	}
	public Integer getEs_renovable() {
		return es_renovable;
	}
	public void setEs_renovable(Integer es_renovable) {
		this.es_renovable = es_renovable;
	}
	public Integer getCodigo_unidad() {
		return codigo_unidad;
	}
	public void setCodigo_unidad(Integer codigo_unidad) {
		this.codigo_unidad = codigo_unidad;
	}
	public Integer getCodigo_usuario() {
		return codigo_usuario;
	}
	public void setCodigo_usuario(Integer codigo_usuario) {
		this.codigo_usuario = codigo_usuario;
	}
	
	
	
	public String getFecha_creacion() {
		return fecha_creacion;
	}
	public void setFecha_creacion(String fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}
	
	public String getFecha_cierre() {
		return fecha_cierre;
	}
	public void setFecha_cierre(String fecha_cierre) {
		this.fecha_cierre = fecha_cierre;
	}
	public String getFecha_apertura_tecnica() {
		return fecha_apertura_tecnica;
	}
	public void setFecha_apertura_tecnica(String fecha_apertura_tecnica) {
		this.fecha_apertura_tecnica = fecha_apertura_tecnica;
	}
	public String getFecha_acto_apertura_economica() {
		return fecha_acto_apertura_economica;
	}
	public void setFecha_acto_apertura_economica(String fecha_acto_apertura_economica) {
		this.fecha_acto_apertura_economica = fecha_acto_apertura_economica;
	}
	public String getFecha_publicacion() {
		return fecha_publicacion;
	}
	public void setFecha_publicacion(String fecha_publicacion) {
		this.fecha_publicacion = fecha_publicacion;
	}
	public String getFecha_adjudicacion() {
		return fecha_adjudicacion;
	}
	public void setFecha_adjudicacion(String fecha_adjudicacion) {
		this.fecha_adjudicacion = fecha_adjudicacion;
	}
	public String getFecha_estimada_adjudicacion() {
		return fecha_estimada_adjudicacion;
	}
	public void setFecha_estimada_adjudicacion(String fecha_estimada_adjudicacion) {
		this.fecha_estimada_adjudicacion = fecha_estimada_adjudicacion;
	}
	
	
	
}
