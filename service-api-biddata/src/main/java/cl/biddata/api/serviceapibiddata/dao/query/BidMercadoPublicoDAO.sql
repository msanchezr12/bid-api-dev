--#addUsuario
insert into usuario_api values (?,?,?,true,?,?,?)

--#getUsuario
select 
rut,
username,
nombre,
apellido_paterno,
apellido_materno,
enabled
from usuario_api
where upper(username) = upper(?)

--#findUserByUsername
select
username,
password,
enabled
from usuario_api 
where upper(username) = upper(?)


--#disableUserApi
update api.usuario_api
set enabled = false
where rut = ?

--#enableUserApi
update api.usuario_api
set enabled = true
where rut = ?

--#getLicitacionById
select
codigo_licitacion,
nombre_licitacion,
codigo_tipo_licitacion,
descripcion_licitacion,
codigo_estado_publicidad_ofertas,
codigo_estado_licitacion,
moneda,
contrato,
obras,
cantidad_reclamos,
codigo_tipo_pago_licitacion,
fuente_financiamiento,
monto_estimado,
nombre_responsable_pago,
email_responsable_pago,
nombre_responsable_contrato,
email_responsable_contrato,
fono_responsable_contrato,
es_renovable,
codigo_unidad,
codigo_usuario,
fecha_creacion,
fecha_cierre,
fecha_apertura_tecnica,
fecha_acto_apertura_economica,
fecha_publicacion,
fecha_adjudicacion
from api.licitacion
where codigo_licitacion = ?
limit 1


--#getLicitacionesByEstado
select 
codigo_licitacion,
nombre_licitacion,
codigo_tipo_licitacion,
descripcion_licitacion,
codigo_estado_publicidad_ofertas,
codigo_estado_licitacion,
moneda,
contrato,
obras,
cantidad_reclamos,
codigo_tipo_pago_licitacion,
fuente_financiamiento,
monto_estimado,
nombre_responsable_pago,
email_responsable_pago,
nombre_responsable_contrato,
email_responsable_contrato,
fono_responsable_contrato,
es_renovable,
codigo_unidad,
codigo_usuario,
fecha_creacion,
fecha_cierre,
fecha_apertura_tecnica,
fecha_acto_apertura_economica,
fecha_publicacion,
fecha_adjudicacion
from licitacion 
where codigo_estado_licitacion = ?
limit 10

--#getItemsByIdLicitacion
select 
it.correlativo_item as correlativo_item,
it.codigo_producto_item as codigo_producto_item,
it.codigo_licitacion as codigo_licitacion, 
it.descripcion_item as descripcion_item,
it.cantidad_item as cantidad_item, 
pr.nombre_producto as nombre_producto, 
pr.unidad_producto as unidad_producto, 
ct.codigo_categoria as codigo_categoria, 
ct.nombre_categoria as nombre_categoria 
from api.item it left join api.producto pr on it.codigo_producto_item = pr.codigo_producto left join api.categoria ct on pr.codigo_categoria_producto = ct.codigo_categoria
where it.codigo_licitacion = ?
order by correlativo_item asc


--#getOfertasByIdLicitacion
select 
correlativo_item_oferta,
codigo_licitacion,
rut_proveedor_oferta,
monto_unitario_oferta,
cantidad_oferta	as cantidad_oferta,
adjudicada as adjudicada,
especificaciones as especificaciones
from api.oferta
where codigo_licitacion = ? and correlativo_item_oferta = ?

--#getLicitacionByFechaAdjudicada
select 
codigo_licitacion,
nombre_licitacion,
codigo_tipo_licitacion,
descripcion_licitacion,
codigo_estado_publicidad_ofertas,
codigo_estado_licitacion,
moneda,
contrato,
obras,
cantidad_reclamos,
codigo_tipo_pago_licitacion,
fuente_financiamiento,
monto_estimado,
nombre_responsable_pago,
email_responsable_pago,
nombre_responsable_contrato,
email_responsable_contrato,
fono_responsable_contrato,
es_renovable,
codigo_unidad,
codigo_usuario,
fecha_creacion,
fecha_cierre,
fecha_apertura_tecnica,
fecha_acto_apertura_economica,
fecha_publicacion,
fecha_adjudicacion
from api.licitacion 
where codigo_estado_licitacion = 8 
and fecha_adjudicacion >= to_timestamp(?,'dd-mm-yyyy hh24:mi:ss')
and fecha_adjudicacion <= to_timestamp(?,'dd-mm-yyyy hh24:mi:ss')


--#getEstados
select 
codigo_estado_licitacion, 
descripcion_estado_licitacion 
from api.estado_licitacion

--#getGarantiaByIdLicitacion
select 
correlativo_garantia,
codigo_licitacion,
glosa_garantia,
monto_garantia,
moneda_garantia,
beneficiario_garantia,
descripcion_garantia,
fecha_vencimiento_garantia,
restitucion_garantia
from 
api.garantia
where codigo_licitacion = ?

--#getCriterioByIdLicitacion
select 
correlativo_criterio,
codigo_licitacion,
items_criterio,
descripcion_criterio,
ponderacion_criterio
from 
api.criterio
where codigo_licitacion = ?

--#getProveedorByRut
select
p.rut_proveedor as rut_proveedor,
p.razon_social_proveedor as razon_social_proveedor,
u.email_usuario_proveedor as email_usuario_proveedor,
u.nombre_usuario_proveedor as nombre_proveedor,
p.region_proveedor as region_proveedor,
p.comuna_proveedor as comuna_proveedor,
p.direccion_proveedor as direccion_proveedor,
p.id_estado_proveedor as id_estado_proveedor,
u.fono1_usuario_proveedor as fono1_usuario_proveedor,
p.link_proveedor as link_proveedor,
p.certificado_socios as certificado_socios,
u.nombre_usuario_proveedor as contacto,
u.fono1_usuario_proveedor as fono_alternativo,
u.email_alternativo_usuario_proveedor as email_alternativo
from api.proveedor p inner join api.usuario_proveedor u on p.rut_proveedor=u.rut_proveedor
where p.rut_proveedor=?
limit 1

--#getLicitacionDetalleMontoById
select monto_estimado_contrato, monto_neto_adjudicado 
from api.licitacion_detalle where codigo_licitacion = ?

--#getLicitacionUnidadDeCompraById
select 
ucom.codigo_unidad,
ucom.codigo_organismo,
ucom.rut_unidad,
ucom.razon_social_unidad,
ucom.nombre_unidad,
ucom.region_unidad,
ucom.comuna_unidad,
ucom.direccion_unidad,
org.nombre_organismo,
org.sector
from api.organismo org left join api.unidad_de_compra ucom on org.codigo_organismo = ucom.codigo_organismo
inner join api.licitacion li on li.codigo_unidad = ucom.codigo_unidad
where li.codigo_licitacion = ? 

--#fin