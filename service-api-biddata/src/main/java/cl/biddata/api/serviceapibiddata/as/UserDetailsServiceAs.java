package cl.biddata.api.serviceapibiddata.as;

//import javax.rmi.CORBA.Util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import cl.biddata.api.serviceapibiddata.dao.AuthenticationDao;
import cl.biddata.api.serviceapibiddata.resource.Util;
import cl.biddata.api.serviceapibiddata.to.BidUsuarioAPI;

@Service
public class UserDetailsServiceAs implements UserDetailsService {

	@Autowired
	private AuthenticationDao authenticationDao;

	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		final BidUsuarioAPI credentials = authenticationDao.findUserByUsername(username);
		if (credentials == null) {
			throw new UsernameNotFoundException(username);
		}
		Util.loggerInfo(0, "username: "+credentials.getUsername());
		Util.loggerInfo(0, "Usuario Inactivo: "+credentials.isEnabled());
		return new User(credentials.getUsername(), credentials.getPassword(), credentials.getAuthorities());
	}
	
}
