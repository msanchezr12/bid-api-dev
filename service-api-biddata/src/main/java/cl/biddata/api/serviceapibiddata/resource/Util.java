package cl.biddata.api.serviceapibiddata.resource;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Util {
	
	private static DevLogger logger = DevLogger.getInstance();
	
	//Logger
	public static final String PATH_PROPERTIES_LOG4J = "/home/biddata/api/api_mp/etc/log4j-bidApiMercadoPublico.properties";
		
	//Properties
	private static final String PATH_FILE_PROPERTIES = "/home/biddata/api/api_mp/etc";
	private static final String FILE_PROPERTIES = "apiBidDataMercadoPublico.properties";
	
	//Estados resultado operacion
	public static final String ESTADO_OK = "Terminado OK - Sin Error";
	public static final String ESTADO_ERROR = "ERROR";
	public static final String ESTADO_FATAL = "FATAL";
	public static final String ESTADO_WARNING = "WARN";
	
	public static final int COD_ERROR = -1;
	public static final int COD_OK = 0;
	
	public static final String LEVEL_LOGGER_DEBUG = "DEBUG";
	public static final String LEVEL_LOGGER_INFO = "INFO";
	
	//Nombre Clases
	public static final String CLASS_STRING = "String";
	public static final String CLASS_INTEGER = "Integer";
	public static final String CLASS_LONG = "Long";
	public static final String CLASS_DATE = "Date";
	
	public static final TimeZone timeZone = TimeZone.getDefault();

	public static final String FORMATO_FECHA_HORA = "dd/MM/yyyy HH:mm:ss";
	public static final String FORMATO_FECHA_HORA_GCALENDAR = "yyyy-MM-dd'T'HH:mm:ss";
	
	//Mensajes exceptiones
	public static final String MSG_FILTROS_BUSQUEDA_INCOMPLETOS = "Debe ingresar los filtros de busqueda obligatorios";
	
	//Mensajes Warning
	public static final String MSG_WARNING_LICITACION_NO_EXISTE = "La Licitacion indicada No Existe";
	public static final String MSG_WARNING_ITEMS_NO_EXISTE = "No existen Items para la licitacion";
	public static final String MSG_WARNING_OFERTAS_NO_EXISTE = "No existen Ofertas para la licitacion";
	public static final String MSG_WARNING_PROVEEDOR_NO_EXISTE = "El Proveedor No Existe";
	public static final String MSG_WARNING_LICITACIONES_NO_EXISTE = "No existen licitaciones para la búsqueda indicada.";
	
	
	public static boolean isNotNull(Object objeto) {
		return (null != objeto) ? true : false;
	}
	
	public static boolean isNotNullString(String value) {
		return (null != value) ? true : false;
	}

	public static boolean isNotNullLong(Long value) {
		return (value!=null) ? true : false;
	}
	
	public static boolean isNotNullDate(Date value) {
		return (value.equals(null)) ? true : false;
	}
	
	
	public static Date fechaIni(Date fecha) {
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		calendar.add(Calendar.HOUR, 0);
		calendar.add(Calendar.MINUTE, 59);
		calendar.add(Calendar.SECOND,59);
		
		Date fecha1 = calendar.getTime();
		return fecha1;
		
	}
	
	public static Date fechaFin(Date fecha) {
		
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(fecha);
		calendar1.add(Calendar.HOUR, 23);
		calendar1.add(Calendar.MINUTE, 59);
		calendar1.add(Calendar.SECOND, 59);
		Date fecha2 = calendar1.getTime();
		return fecha2;

	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String convertFormatCurrency(double value) {
		
		Locale locale = new Locale("es","CL");
		NumberFormat nf = NumberFormat.getCurrencyInstance(locale);				
		return nf.format(value);
	}
	
	
	public static double convetString2double(String value) {
		
		double var=0;
		try {
				var= Double.parseDouble(value);
		} catch (Exception e) {
			logger.warn("Ocurrio un error al convertir a String to Double");
		}
		return var;
	}
	
	
	
	/**
	 * Metodo que permite convertir una fecha en formato Date a String
	 * 
	 * @param fecha
	 * @return
	 */
	public static String convertDateToStr(Date fecha) {
		
		DateFormat dateformat = new SimpleDateFormat(FORMATO_FECHA_HORA);
		String strfecha = null;
		
		try {
			if (Util.isNotNull(fecha)) {
				strfecha = dateformat.format(fecha);	
			}
		} catch (Exception e) {
			logger.warn("Ocurrio un error al convertir a String la fecha");
		}
		
		return strfecha;
	}
	
	/**
	 * Metodo que permite loggear los atributos de un objeto con nivel DEBUG
	 * 
	 * @param object
	 * @param idEvento
	 */
	public static void loggerDebugParametersObject(Object object, long idEvento) {
		
		try {
			
			loggerParametersObject(object, idEvento, Util.LEVEL_LOGGER_DEBUG);
			
		} catch (Exception e) {
			loggerWarn(idEvento, "Error en logger de los parametros, Exception: " + e.getMessage());
		}
	}
	
		
	/**
	 * Metodo que permite loggear los atributos de un objeto dependiendo del nivel de logeo
	 * 
	 * @param object
	 * @param idEvento
	 * @param typeLogger
	 * @throws Exception
	 */
	public static void loggerParametersObject(Object object, long idEvento, String typeLogger) 
		throws Exception{
		
		try {
			
			Class<?> clazz = object.getClass();
			Field[] fields = clazz.getDeclaredFields();
			String type;
			boolean isAccessible;
		
			for (Field field : fields) {
				
				type = field.getType().getSimpleName();
				if (type.equals(CLASS_STRING) 
						|| type.equals(CLASS_INTEGER) 
							|| type.equals(CLASS_LONG)
								|| type.equals(CLASS_DATE)) {

					isAccessible = field.isAccessible();
					if (!isAccessible) {
						field.setAccessible(true);	
					}
					
					if (isNotNull(field.get(object)) && (!("").equals(field.get(object)))) {
						
						if (Util.LEVEL_LOGGER_DEBUG.equals(typeLogger)) {
							loggerDebug(idEvento, clazz.getSimpleName() + ", " + field.getName() + ": " + field.get(object));
						
						} else if (Util.LEVEL_LOGGER_INFO.equals(typeLogger)) {
							loggerInfo(idEvento, clazz.getSimpleName() + ", " + field.getName() + ": " + field.get(object));
						}
					}
	
					if (!isAccessible) {
						field.setAccessible(false);	
					}
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Metodo que permite obtener un identificador unico de transaccion.
	 * 
	 * @return long
	 */
	public static long obtenerIdEvento() {
		return new Date().getTime();
	}	
	
	public static void loggerInfo(long idEvento, String mensaje) {
		logger.info("[ Key " + idEvento + " ] " + mensaje);
	}
	
	public static void loggerDebug(long idEvento, String mensaje) {
		logger.debug("[ Key " + idEvento + " ] " + mensaje);
	}
	
	public static void loggerWarn(long idEvento, String mensaje) {
		logger.warn("[ Key " + idEvento + " ] " + mensaje);
	}
	
	public static void loggerError(long idEvento, String mensaje) {
		logger.error("[ Key " + idEvento + " ] " + mensaje);
	}
	
	public static void loggerError(long idEvento, String mensaje, String excepcion) {
		logger.error("[ Key " + idEvento + " ] " + mensaje + ", Excepcion: " + excepcion);
	}

	
}
