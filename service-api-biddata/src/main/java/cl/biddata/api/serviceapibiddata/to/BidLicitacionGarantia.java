package cl.biddata.api.serviceapibiddata.to;

public class BidLicitacionGarantia {

	private Integer correlativo_garantia;		//" int4 NOT NULL,
	private String codigo_licitacion;			//" varchar(50) COLLATE "default" NOT NULL,
	private String glosa_garantia;				//" varchar(4000) COLLATE "default",
	private Long monto_garantia;				//" float8,
	private String moneda_garantia; 			//" varchar(50) COLLATE "default",
	private String beneficiario_garantia; 		//varchar(500) COLLATE "default",
	private String descripcion_garantia; 		//varchar(4000) COLLATE "default",
	private String fecha_vencimiento_garantia;	//" varchar(10) COLLATE "default",
	private String restitucion_garantia; 		//varchar(4000) COLLATE "default"
	
	
	public Integer getCorrelativo_garantia() {
		return correlativo_garantia;
	}
	public void setCorrelativo_garantia(Integer correlativo_garantia) {
		this.correlativo_garantia = correlativo_garantia;
	}
	public String getCodigo_licitacion() {
		return codigo_licitacion;
	}
	public void setCodigo_licitacion(String codigo_licitacion) {
		this.codigo_licitacion = codigo_licitacion;
	}
	public String getGlosa_garantia() {
		return glosa_garantia;
	}
	public void setGlosa_garantia(String glosa_garantia) {
		this.glosa_garantia = glosa_garantia;
	}
	public Long getMonto_garantia() {
		return monto_garantia;
	}
	public void setMonto_garantia(Long monto_garantia) {
		this.monto_garantia = monto_garantia;
	}
	public String getMoneda_garantia() {
		return moneda_garantia;
	}
	public void setMoneda_garantia(String moneda_garantia) {
		this.moneda_garantia = moneda_garantia;
	}
	public String getBeneficiario_garantia() {
		return beneficiario_garantia;
	}
	public void setBeneficiario_garantia(String beneficiario_garantia) {
		this.beneficiario_garantia = beneficiario_garantia;
	}
	public String getDescripcion_garantia() {
		return descripcion_garantia;
	}
	public void setDescripcion_garantia(String descripcion_garantia) {
		this.descripcion_garantia = descripcion_garantia;
	}
	public String getFecha_vencimiento_garantia() {
		return fecha_vencimiento_garantia;
	}
	public void setFecha_vencimiento_garantia(String fecha_vencimiento_garantia) {
		this.fecha_vencimiento_garantia = fecha_vencimiento_garantia;
	}
	public String getRestitucion_garantia() {
		return restitucion_garantia;
	}
	public void setRestitucion_garantia(String restitucion_garantia) {
		this.restitucion_garantia = restitucion_garantia;
	}
	
}

