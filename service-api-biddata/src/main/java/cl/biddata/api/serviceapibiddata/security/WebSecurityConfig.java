package cl.biddata.api.serviceapibiddata.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailsService userDetailService;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		http.cors().and().csrf().disable().authorizeRequests()
//		.antMatchers("/").permitAll()
//		.antMatchers(HttpMethod.POST,
//				"/biddata-api-service/add"
//				).permitAll()
//		.antMatchers(HttpMethod.GET,
//		"/ApiDocumentation.json"
//				).permitAll()
//		.anyRequest().authenticated()
//		
//		.and()
//		.addFilterBefore(new JWTLoginFilter("/auth/login", authenticationManager()),
//				UsernamePasswordAuthenticationFilter.class)
//		.addFilterBefore(new JWTAuthenticationFilter(),	
//				UsernamePasswordAuthenticationFilter.class)
//		// this disables session creation on Spring Security
//		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
//	}
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().authorizeRequests()
		.antMatchers("/").permitAll()
		.antMatchers(HttpMethod.POST,
				"/biddata-api-service/add"
				).permitAll()
		.antMatchers(HttpMethod.GET,
				"/biddata-api-service/licitacion/{id}",
				"/biddata-api-service/licitacion/adjudicada/{fecha}",
				"/biddata-api-service/proveedor/{rut}",
				"/ApiDocumentation.json"
				).permitAll()
		.anyRequest().authenticated()
		
		.and()
		.addFilterBefore(new JWTLoginFilter("/auth/login", authenticationManager()),
				UsernamePasswordAuthenticationFilter.class)
		.addFilterBefore(new JWTAuthenticationFilter(),	
				UsernamePasswordAuthenticationFilter.class)
		// this disables session creation on Spring Security
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
		
	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
		return source;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailService).passwordEncoder(bCryptPasswordEncoder);
	}
}
