package cl.biddata.api.serviceapibiddata.to;

public class BidLicitacionProveedor {
	
	protected String rut_proveedor;				// varchar(12) COLLATE "default" NOT NULL,
	protected Integer cod_sucursal; 			//" int4,
	protected String razon_social_proveedor; 	//" varchar(255) COLLATE "default",
	protected String nombre_proveedor; 			// " varchar(255) COLLATE "default",
	protected String region_proveedor; 			//" varchar(50) COLLATE "default",
	protected String comuna_proveedor; 			//" varchar(50) COLLATE "default",
	protected String direccion_proveedor; 		//" varchar(255) COLLATE "default",
	protected Integer id_estado_proveedor; 		//" int4,
	protected String certificado_socios;		//" varchar(255) COLLATE "default",
	protected String link_proveedor;			//" varchar(500) COLLATE "default"	
	
	
	public String getRut_proveedor() {
		return rut_proveedor;
	}
	public void setRut_proveedor(String rut_proveedor) {
		this.rut_proveedor = rut_proveedor;
	}
	public Integer getCod_sucursal() {
		return cod_sucursal;
	}
	public void setCod_sucursal(Integer cod_sucursal) {
		this.cod_sucursal = cod_sucursal;
	}
	public String getRazon_social_proveedor() {
		return razon_social_proveedor;
	}
	public void setRazon_social_proveedor(String razon_social_proveedor) {
		this.razon_social_proveedor = razon_social_proveedor;
	}
	public String getNombre_proveedor() {
		return nombre_proveedor;
	}
	public void setNombre_proveedor(String nombre_proveedor) {
		this.nombre_proveedor = nombre_proveedor;
	}
	public String getRegion_proveedor() {
		return region_proveedor;
	}
	public void setRegion_proveedor(String region_proveedor) {
		this.region_proveedor = region_proveedor;
	}
	public String getComuna_proveedor() {
		return comuna_proveedor;
	}
	public void setComuna_proveedor(String comuna_proveedor) {
		this.comuna_proveedor = comuna_proveedor;
	}
	public String getDireccion_proveedor() {
		return direccion_proveedor;
	}
	public void setDireccion_proveedor(String direccion_proveedor) {
		this.direccion_proveedor = direccion_proveedor;
	}
	public Integer getId_estado_proveedor() {
		return id_estado_proveedor;
	}
	public void setId_estado_proveedor(Integer id_estado_proveedor) {
		this.id_estado_proveedor = id_estado_proveedor;
	}
	public String getCertificado_socios() {
		return certificado_socios;
	}
	public void setCertificado_socios(String certificado_socios) {
		this.certificado_socios = certificado_socios;
	}
	public String getLink_proveedor() {
		return link_proveedor;
	}
	public void setLink_proveedor(String link_proveedor) {
		this.link_proveedor = link_proveedor;
	}

}
