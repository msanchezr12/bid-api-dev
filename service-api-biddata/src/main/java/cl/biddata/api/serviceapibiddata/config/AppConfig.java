package cl.biddata.api.serviceapibiddata.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;

import com.google.common.collect.Lists;

import cl.biddata.api.serviceapibiddata.dao.QueryProvider;
import io.jsonwebtoken.lang.Arrays;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@PropertySource("classpath:application.properties")
public class AppConfig  {

	@Value("${com.datasource.bid.driverClass}")
	private String driverClass;
	@Value("${com.datasource.bid.url}")
	private String dbUrl;
	@Value("${com.datasource.bid.username}")
	private String dbUser;
	@Value("${com.datasource.bid.password}")
	private String dbPass;


    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                	.apis(RequestHandlerSelectors.basePackage("cl.biddata.api.serviceapibiddata.controller"))
                	.paths(PathSelectors.regex("/biddata-api-service.*"))
                	.build();
    }
      		
	@Bean
	public DataSource dataSource() {
		
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(driverClass);
		dataSource.setUrl(dbUrl);
		dataSource.setUsername(dbUser);
		dataSource.setPassword(dbPass);
		
		return dataSource;
	}
	
	@Bean
	public SimpleJdbcTemplate bidSimpleJdbcTempleate() {
		return new SimpleJdbcTemplate(dataSource());
		
	}
	
    @Bean
    public PlatformTransactionManager transactionManager() {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSource());
        return transactionManager;
    }
	
	@Bean
	public QueryProvider provider(){
		return new QueryProvider();
	}
	
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
    	return new BCryptPasswordEncoder();
    }
    
}
