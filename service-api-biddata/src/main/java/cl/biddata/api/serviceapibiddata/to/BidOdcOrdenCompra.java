package cl.biddata.api.serviceapibiddata.to;

public class BidOdcOrdenCompra {
	
	
protected String codigo; 			//" varchar(50) COLLATE "default" NOT NULL,
protected String nombre; 			//" varchar(4000) COLLATE "default",
protected Integer codigoestado;			//" int4,
protected String estado;			//" varchar(50) COLLATE "default",
protected String codigolicitacion; 	//" varchar(50) COLLATE "default",
protected String descripcion; 		//" varchar(4000) COLLATE "default",
protected Integer codigotipo;			//" int4,
protected String tipo; //" varchar(3) COLLATE "default",
protected String tipomoneda; //" varchar(3) COLLATE "default",
protected Integer codigoestadoproveedor; //" int4,
protected String estadoproveedor; //" varchar(50) COLLATE "default",
protected String tieneitems; //" varchar(3) COLLATE "default",
protected Long promediocalificacion; //" float8,
protected Integer cantidadevaluacion; // " int4,
protected Long descuentos; //" float8,
protected Long cargos; //" float8,
protected Long totalneto; //" float8,
protected Long porcentajeiva; //" float8,
protected Long impuestos; //" float8,
protected Long total; // float8,
protected String financiamiento; //" varchar(1000) COLLATE "default",
protected String pais; //" varchar(10) COLLATE "default",
protected String tipodespacho; //" varchar(2) COLLATE "default",
protected String formapago; //" varchar(3) COLLATE "default",
protected String fechacreacion; // " timestamp(6),
protected String fechaenvio; //" timestamp(6),
protected String fechaaceptacion; // timestamp(6),
protected String fechacancelacion; //" timestamp(6),
protected String fechaultimamodificacion; //" timestamp(6),
protected Integer codigounidad; //" int4 NOT NULL,
protected Integer codigoorganismo; //" int4 NOT NULL,
protected Integer codigosucursal; //" int4 NOT NULL,
protected Integer codigoproveedor; //" int4 NOT NULL

public String getCodigo() {
	return codigo;
}
public void setCodigo(String codigo) {
	this.codigo = codigo;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public Integer getCodigoestado() {
	return codigoestado;
}
public void setCodigoestado(Integer codigoestado) {
	this.codigoestado = codigoestado;
}
public String getEstado() {
	return estado;
}
public void setEstado(String estado) {
	this.estado = estado;
}
public String getCodigolicitacion() {
	return codigolicitacion;
}
public void setCodigolicitacion(String codigolicitacion) {
	this.codigolicitacion = codigolicitacion;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public Integer getCodigotipo() {
	return codigotipo;
}
public void setCodigotipo(Integer codigotipo) {
	this.codigotipo = codigotipo;
}
public String getTipo() {
	return tipo;
}
public void setTipo(String tipo) {
	this.tipo = tipo;
}
public String getTipomoneda() {
	return tipomoneda;
}
public void setTipomoneda(String tipomoneda) {
	this.tipomoneda = tipomoneda;
}
public Integer getCodigoestadoproveedor() {
	return codigoestadoproveedor;
}
public void setCodigoestadoproveedor(Integer codigoestadoproveedor) {
	this.codigoestadoproveedor = codigoestadoproveedor;
}
public String getEstadoproveedor() {
	return estadoproveedor;
}
public void setEstadoproveedor(String estadoproveedor) {
	this.estadoproveedor = estadoproveedor;
}
public String getTieneitems() {
	return tieneitems;
}
public void setTieneitems(String tieneitems) {
	this.tieneitems = tieneitems;
}
public Long getPromediocalificacion() {
	return promediocalificacion;
}
public void setPromediocalificacion(Long promediocalificacion) {
	this.promediocalificacion = promediocalificacion;
}
public Integer getCantidadevaluacion() {
	return cantidadevaluacion;
}
public void setCantidadevaluacion(Integer cantidadevaluacion) {
	this.cantidadevaluacion = cantidadevaluacion;
}
public Long getDescuentos() {
	return descuentos;
}
public void setDescuentos(Long descuentos) {
	this.descuentos = descuentos;
}
public Long getCargos() {
	return cargos;
}
public void setCargos(Long cargos) {
	this.cargos = cargos;
}
public Long getTotalneto() {
	return totalneto;
}
public void setTotalneto(Long totalneto) {
	this.totalneto = totalneto;
}
public Long getPorcentajeiva() {
	return porcentajeiva;
}
public void setPorcentajeiva(Long porcentajeiva) {
	this.porcentajeiva = porcentajeiva;
}
public Long getImpuestos() {
	return impuestos;
}
public void setImpuestos(Long impuestos) {
	this.impuestos = impuestos;
}
public Long getTotal() {
	return total;
}
public void setTotal(Long total) {
	this.total = total;
}
public String getFinanciamiento() {
	return financiamiento;
}
public void setFinanciamiento(String financiamiento) {
	this.financiamiento = financiamiento;
}
public String getPais() {
	return pais;
}
public void setPais(String pais) {
	this.pais = pais;
}
public String getTipodespacho() {
	return tipodespacho;
}
public void setTipodespacho(String tipodespacho) {
	this.tipodespacho = tipodespacho;
}
public String getFormapago() {
	return formapago;
}
public void setFormapago(String formapago) {
	this.formapago = formapago;
}
public String getFechacreacion() {
	return fechacreacion;
}
public void setFechacreacion(String fechacreacion) {
	this.fechacreacion = fechacreacion;
}
public String getFechaenvio() {
	return fechaenvio;
}
public void setFechaenvio(String fechaenvio) {
	this.fechaenvio = fechaenvio;
}
public String getFechaaceptacion() {
	return fechaaceptacion;
}
public void setFechaaceptacion(String fechaaceptacion) {
	this.fechaaceptacion = fechaaceptacion;
}
public String getFechacancelacion() {
	return fechacancelacion;
}
public void setFechacancelacion(String fechacancelacion) {
	this.fechacancelacion = fechacancelacion;
}
public String getFechaultimamodificacion() {
	return fechaultimamodificacion;
}
public void setFechaultimamodificacion(String fechaultimamodificacion) {
	this.fechaultimamodificacion = fechaultimamodificacion;
}
public Integer getCodigounidad() {
	return codigounidad;
}
public void setCodigounidad(Integer codigounidad) {
	this.codigounidad = codigounidad;
}
public Integer getCodigoorganismo() {
	return codigoorganismo;
}
public void setCodigoorganismo(Integer codigoorganismo) {
	this.codigoorganismo = codigoorganismo;
}
public Integer getCodigosucursal() {
	return codigosucursal;
}
public void setCodigosucursal(Integer codigosucursal) {
	this.codigosucursal = codigosucursal;
}
public Integer getCodigoproveedor() {
	return codigoproveedor;
}
public void setCodigoproveedor(Integer codigoproveedor) {
	this.codigoproveedor = codigoproveedor;
}
	


}
