package cl.biddata.api.serviceapibiddata.as;

import java.util.List;

import cl.biddata.api.serviceapibiddata.to.BidLicitacionItem;

public class BidItemsResponse {

	
	protected List<BidLicitacionItem> itemsList;
	private Integer codigo_error;
	private String mensaje_error;
	
	public List<BidLicitacionItem> getItemsList() {
		return itemsList;
	}
	public void setItemsList(List<BidLicitacionItem> itemsList) {
		this.itemsList = itemsList;
	}
	public Integer getCodigo_error() {
		return codigo_error;
	}
	public void setCodigo_error(Integer codigo_error) {
		this.codigo_error = codigo_error;
	}
	public String getMensaje_error() {
		return mensaje_error;
	}
	public void setMensaje_error(String mensaje_error) {
		this.mensaje_error = mensaje_error;
	}
	
	
	
	
}
