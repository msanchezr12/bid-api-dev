package cl.biddata.api.serviceapibiddata.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.stereotype.Repository;
import cl.biddata.api.serviceapibiddata.to.BidUsuarioAPI;

@Repository
public class AuthenticationDao {
	@Autowired
	private SimpleJdbcTemplate bidSimpleJdbcTemplate;
	
	@Autowired
	private QueryProvider provider;

	public BidUsuarioAPI findUserByUsername(String username)  {
		try {
			List<BidUsuarioAPI> list = bidSimpleJdbcTemplate.query(provider.getQuery("findUserByUsername"), new BeanPropertyRowMapper<BidUsuarioAPI>(BidUsuarioAPI.class), new Object[]{ username });
			return list != null && !list.isEmpty() ? list.get(0): null;
		} catch (Exception e) {
			throw e;
		}
	}

}
