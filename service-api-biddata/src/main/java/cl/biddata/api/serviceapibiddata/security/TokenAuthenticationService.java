package cl.biddata.api.serviceapibiddata.security;

import static java.util.Collections.emptyList;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import cl.biddata.api.serviceapibiddata.resource.Util;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenAuthenticationService {
	static final long EXPIRATIONTIME = 1_800_000; // 10 days
	static final String SECRET = "ThisIsASecret";
	static final String HEADER_STRING = "Authorization";

	static void addAuthentication(HttpServletResponse res, String username) throws IOException {
		String JWT = Jwts.builder()
				.setSubject(username)
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME*2250))
				.signWith(SignatureAlgorithm.HS512, SECRET)
				.compact();
		res.addHeader(HEADER_STRING, JWT);
		res.getWriter().println("{\""+HEADER_STRING+"\" : \""+JWT+"\"}");
	}

	static Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(HEADER_STRING);
		if (token != null) {
			// parse the token.
			String user = null;
			try {
				user = Jwts.parser()
						.setSigningKey(SECRET)
						.parseClaimsJws(token)
						.getBody()
						.getSubject();
				
			} catch (JwtException e) {
				Util.loggerError(0, "Error parse the token"+e);
				System.out.println(e.getMessage());
			}
			
			return user != null ? new UsernamePasswordAuthenticationToken(user, null, emptyList()) : null;
		}
		return null;
	}
}
