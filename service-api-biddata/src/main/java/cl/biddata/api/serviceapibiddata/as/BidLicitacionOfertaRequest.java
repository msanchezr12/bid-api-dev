package cl.biddata.api.serviceapibiddata.as;

public class BidLicitacionOfertaRequest {

	protected String codigo_licitacion;
	protected Integer correlativo_item;
	
	public String getCodigo_licitacion() {
		return codigo_licitacion;
	}
	public void setCodigo_licitacion(String codigo_licitacion) {
		this.codigo_licitacion = codigo_licitacion;
	}
	public Integer getCorrelativo_item() {
		return correlativo_item;
	}
	public void setCorrelativo_item(Integer correlativo_item) {
		this.correlativo_item = correlativo_item;
	}
	
	
	
	
}
