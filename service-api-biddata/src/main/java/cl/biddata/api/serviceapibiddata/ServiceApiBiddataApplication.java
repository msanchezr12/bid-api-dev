package cl.biddata.api.serviceapibiddata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceApiBiddataApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceApiBiddataApplication.class, args);
	}
}
