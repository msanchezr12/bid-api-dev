package cl.biddata.api.serviceapibiddata.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import cl.biddata.api.serviceapibiddata.dao.QueryProvider;

public class QueryProvider {
	
	private static Log log = LogFactory.getLog(QueryProvider.class);
	private ConcurrentHashMap<String, ConcurrentHashMap<String, String>> packages = new ConcurrentHashMap<String, ConcurrentHashMap<String, String>>();
	private final transient PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
	private transient String thisPackage;

	private PathMatchingResourcePatternResolver getResolver() {
		return this.resolver;
	}

	public QueryProvider() {
		try {
			this.thisPackage = getCurrentPackage(getClass());
		} catch (Exception e) {
			log.error("[" + getClass() + "]" + " / Error al cargar Package: " + e.getMessage());
		}
	}

	public String getQuery() {
		StackTraceElement stack = Thread.currentThread().getStackTrace()[3];
		String methodName = stack.getMethodName();
		log.debug(methodName);
		return getQuery(methodName);
	}

	public String getQuery(String query) {
		String queryReturn = null;
		try {
			if (this.packages.get(this.thisPackage) == null) {
				obtainQueries(this.thisPackage);
			}
			queryReturn = (String) ((ConcurrentHashMap<?, ?>) this.packages.get(this.thisPackage)).get(query);
		} catch (Exception e) {
			log.error("Error al obtener la query de nombre = [" + query + "], se retorna null");
		}
		return queryReturn;
	}

	public String getQueryWithReplacement(String query, String replacement) {
		String queryReturn = getQuery(query);
		return queryReturn != null ? queryReturn.replaceAll("\\##", replacement) : null;
	}

	private void obtainQueries(String currentPackage) throws Exception {
		if (log.isInfoEnabled()) {
			log.info("Iniciando obtainQueries...");
		}
		Resource[] resources = new Resource[0];
		ConcurrentHashMap<String, String> queries = new ConcurrentHashMap<String, String>();
		try {
			resources = getResolver().getResources("classpath*:" + currentPackage + "/**/*.sql");
			if (log.isDebugEnabled()) {
				log.debug("Se obtuvieron " + (resources == null ? 0 : resources.length) + " archivos");
			}
			List<String> lines = getFilesLines(resources);
			StringBuffer query = new StringBuffer();

			String queryName = "";
			for (String line : lines) {
				String newQueryName = getQueryName(line);
				if (newQueryName != null) {
					if (log.isDebugEnabled()) {
						log.debug("Nueva query [" + newQueryName + "]");
					}
					if (query.length() > 0) {
						queries.put(queryName, query.toString().trim());
						if (log.isDebugEnabled()) {
							log.debug("Query almacenada [" + queryName + "][" + query.toString().trim() + "]");
						}
					}
					query.setLength(0);
					queryName = newQueryName;
				} else {
					String newLine = removeComment(line);
					if (newLine.length() > 0) {
						query.append(newLine).append(" ");
					}
				}
			}
			this.packages.put(currentPackage, queries);
		} catch (IOException e) {
			log.warn("AL obtener queries : " + e.getMessage());
			resources = new Resource[0];
		}
	}

	private List<String> getFilesLines(Resource[] resources) {
		List<String> lines = new ArrayList<String>();
		Resource[] arr$ = resources;
		int len$ = arr$.length;
		for (int i$ = 0; i$ < len$;) {
			Resource resource = arr$[i$];
			if (log.isDebugEnabled()) {
				log.debug("Obteniendo lineas de archivo: " + resource.getFilename());
			}
			InputStream is = null;
			try {
				is = resource.getInputStream();
				if (is == null) {
					return lines;
				}
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				String line;
				while ((line = reader.readLine()) != null) {
					lines.add(line);
				}
				try {
					is.close();
				} catch (Exception e) {
					log.warn("Error al cerrar inputStram : " + e.getMessage());
				}
				i$++;
			} catch (Exception e) {
				log.warn("Error al Obtener lineas : " + e.getMessage());
			} finally {
				try {
					is.close();
				} catch (Exception e) {
					log.warn("Error al cerrar inputStram : " + e.getMessage());
				}
			}
		}
		return lines;
	}

	private static String removeComment(String line) {
		int comment = line.indexOf("--");
		String out = null;
		if (comment < 0) {
			out = line.trim();
		} else {
			out = line.substring(0, comment).trim();
		}
		return out;
	}

	private String getQueryName(String line) {
		String out = null;
		if (line.startsWith("--#")) {
			out = line.substring(3);
		}
		return out;
	}

	private String getCurrentPackage(Class<?> currentClass) throws Exception {
		String packgeReturn = null;
		try {
			if (log.isDebugEnabled()) {
				log.debug("Inicio, obtener packages de clase:  " + currentClass);
			}
			packgeReturn = currentClass.getPackage().getName().replaceAll("\\.", "/");
			if (log.isDebugEnabled()) {
				log.debug("Package Obtenido : [ " + packgeReturn + " ]");
			}
		} catch (NullPointerException e) {
			log.error("Error, Entrada nula. Tarce: " + e.getMessage());
			throw new Exception("Error, la clase provista es nula.", e);
		} catch (Exception e) {
			log.error("Error al intentar obtener el package de la clase. Tarce: " + e.getMessage());
			throw new Exception("Error al obtener package de la clase. Trace: ", e);
		}
		return packgeReturn;
	}

}
