package cl.biddata.api.serviceapibiddata.to;

import java.util.ArrayList;
import java.util.List;

public class BidLicitacionesBasicas {

	protected List<BidLicitacionBasica> licitacionesBasicas;

	public List<BidLicitacionBasica> getLicitacionesBasicasList() {
		return licitacionesBasicas;
	}

	public void setLicitacionesBasicasList(List<BidLicitacionBasica> licitacionesBasicas) {
		this.licitacionesBasicas = new ArrayList<BidLicitacionBasica>(licitacionesBasicas);
	}

	
}
