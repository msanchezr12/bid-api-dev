package cl.biddata.api.serviceapibiddata.as;

import cl.biddata.api.serviceapibiddata.to.BidProveedor;

public class BidProveedorResponse {

	BidProveedor bidProveedor;
	
	private Integer codigo_error;
	private String mensaje_error;
	
	public BidProveedor getBidproveedor() {
		return bidProveedor;
	}
	public void setBidProveedor(BidProveedor bidproveedor) {
		this.bidProveedor = bidproveedor;
	}
	public Integer getCodigo_error() {
		return codigo_error;
	}
	public void setCodigo_error(Integer codigo_error) {
		this.codigo_error = codigo_error;
	}
	public String getMensaje_error() {
		return mensaje_error;
	}
	public void setMensaje_error(String mensaje_error) {
		this.mensaje_error = mensaje_error;
	}
	
	
	
	
	
	
}
