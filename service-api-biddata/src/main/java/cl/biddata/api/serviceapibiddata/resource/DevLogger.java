package cl.biddata.api.serviceapibiddata.resource;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class DevLogger {

	private static DevLogger devLogger = new DevLogger();
	
	private Logger logger;
	
	private DevLogger() {
		String log4jConfigFile = Util.PATH_PROPERTIES_LOG4J;
		PropertyConfigurator.configure(log4jConfigFile);
		logger = Logger.getLogger(DevLogger.class);
	}
	
	public static DevLogger getInstance() {
		return devLogger;
	}

	public void debug(String message) {
		logger.debug(message);
	}

	public void error(String message) {
		logger.error(message);
	}
	
	public void info(String message) {
		logger.info(message);
	}
	
	public void warn(String message) {
		logger.warn(message);
	}
	
}
