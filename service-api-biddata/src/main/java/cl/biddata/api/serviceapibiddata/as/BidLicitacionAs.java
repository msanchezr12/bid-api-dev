package cl.biddata.api.serviceapibiddata.as;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import cl.biddata.api.serviceapibiddata.dao.BidMercadoPublicoDAO;
import cl.biddata.api.serviceapibiddata.exception.BidServiceException;
import cl.biddata.api.serviceapibiddata.resource.Util;
import cl.biddata.api.serviceapibiddata.service.BidLicitacionService;
import cl.biddata.api.serviceapibiddata.to.BidLicitacion;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionBasica;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionCompleta;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionCriterio;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionCriterios;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionGarantia;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionGarantias;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionItem;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionItems;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionDetalleMonto;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionDetalleMontos;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionOferta;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionOfertas;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionUnidadDeCompra;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionUnidadDeCompras;
import cl.biddata.api.serviceapibiddata.to.BidLicitacionesBasicas;
import cl.biddata.api.serviceapibiddata.to.BidProveedor;
import cl.biddata.api.serviceapibiddata.to.BidProveedores;
import cl.biddata.api.serviceapibiddata.to.BidUsuarioAPI;


@Service
public class BidLicitacionAs implements BidLicitacionService {
	
	@Autowired
	private BidMercadoPublicoDAO bidMercadoPublicoDao;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public BidUsuarioAPI getUsuarioApiByRut(String rut) throws BidServiceException {
		long idEvento = Util.obtenerIdEvento();
		Util.loggerInfo(idEvento, "get person rut  [" + rut + "]");
		return bidMercadoPublicoDao.getUsuarioApiByRut(rut,idEvento);
	}

	
	@Override
	public void addUsuarioAPI(BidUsuarioAPI person) throws BidServiceException {
		long idEvento = Util.obtenerIdEvento();
		Util.loggerInfo(idEvento, "add new person"+person.getRut());
		try {
			person.setPassword(bCryptPasswordEncoder.encode(person.getPassword()));
			bidMercadoPublicoDao.addUsuarioAPI(person, idEvento);
		} catch (BidServiceException e) {
			Util.loggerError(idEvento, "Error add new person"+e);
		} catch (Exception e) {
			Util.loggerError(idEvento, "Error add new person"+e);
		}
	}
	
	public BidOdcOrdenCompraCompletaResponse getOrdenCompraById(String id) throws BidServiceException {
		
		long idEvento = Util.obtenerIdEvento();
		BidOdcOrdenCompraCompletaResponse orderCompraResponse = new BidOdcOrdenCompraCompletaResponse();
		
		try {
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return orderCompraResponse;
	}
	
		
	@Override
	public BidLicitacionCompletaResponse getLicitacionById(String id) throws BidServiceException {
		long idEvento = Util.obtenerIdEvento();
		Util.loggerInfo(idEvento, "Inicio servicio api getLicitacionById");

		BidLicitacionCompletaResponse licitacionResponse = new BidLicitacionCompletaResponse();
		
		// Datos Licitacion
		BidLicitacionCompleta rsLicitacionCompleta = new BidLicitacionCompleta();
		BidLicitacion rsLicitacion = new BidLicitacion();
		
		// Datos Items
		BidLicitacionItems rsLicitacionItems;
		List<BidLicitacionItem> rsListaItems;
		List<BidLicitacionItem> licitacionItems;
		BidLicitacionItem rsLicitacionItem;
		
		// Datos Ofertas
		BidLicitacionOfertaRequest parameterOferta;
		BidLicitacionOfertas rsLicitacionOfertas;
		List<BidLicitacionOferta> licitacionOfertas;
		List<BidLicitacionOferta> rsListaOfertas;
		BidLicitacionOferta rsLicitacionOferta;
		
		// Datos Garantia
		BidLicitacionGarantias rsLicitacionGarantias;
		List<BidLicitacionGarantia> rsListaGarantias;
		List<BidLicitacionGarantia> licitacionGarantias;
		BidLicitacionGarantia rsLicitacionGarantia;
		
		//Datos Criterios
		BidLicitacionCriterios rsLictacionCriterios;
		List<BidLicitacionCriterio> rsListaCriterios;
		List<BidLicitacionCriterio> licitacionCriterios;
		BidLicitacionCriterio rsLicitacionCriterio;
		
		// Dato Montos
		BidLicitacionDetalleMontos rsLicitacionMontos;
		List<BidLicitacionDetalleMonto> rsListaMontos;
		List<BidLicitacionDetalleMonto> licitacionDetalleMontos;
		BidLicitacionDetalleMonto rsLicitacionMonto;
		
		// Datos Organismo
		BidLicitacionUnidadDeCompras rsLicitacionUnidadesDeCompras;
		List<BidLicitacionUnidadDeCompra> rsListaUnidadesDeCompras;
		List<BidLicitacionUnidadDeCompra> licitacionUnidadesDeCompra;
		BidLicitacionUnidadDeCompra rsLicitacionUnidadDeCompra;
		
		try {
			
			// Buscamos datos pertenecientes a la Licitacion
			Util.loggerInfo(idEvento, "Obteniendo Datos Licitacion... ");
			rsLicitacion = bidMercadoPublicoDao.getLicitacionById(id,idEvento);
		
			if(Util.isNotNull(rsLicitacion)&&!rsLicitacion.getCodigo_licitacion().equals("-1")) {
				
				Util.loggerInfo(idEvento, "Obteniendo Datos Licitacion... OK");
				rsLicitacionCompleta.setCodigo_licitacion(rsLicitacion.getCodigo_licitacion());
				rsLicitacionCompleta.setCantidad_reclamos(rsLicitacion.getCantidad_reclamos());
				rsLicitacionCompleta.setNombre_licitacion(rsLicitacion.getNombre_licitacion());
				rsLicitacionCompleta.setCodigo_tipo_licitacion(rsLicitacion.getCodigo_tipo_licitacion());
				rsLicitacionCompleta.setDescripcion_licitacion(rsLicitacion.getDescripcion_licitacion());
				rsLicitacionCompleta.setCodigo_estado_publicidad_ofertas(rsLicitacion.getCodigo_estado_publicidad_ofertas());
				rsLicitacionCompleta.setCodigo_estado_licitacion(rsLicitacion.getCodigo_estado_licitacion());
				rsLicitacionCompleta.setMoneda(rsLicitacion.getMoneda());
				rsLicitacionCompleta.setContrato(rsLicitacion.getContrato());
				rsLicitacionCompleta.setObras(rsLicitacion.getObras());
				rsLicitacionCompleta.setCantidad_reclamos(rsLicitacion.getCantidad_reclamos());
				rsLicitacionCompleta.setCodigo_tipo_licitacion(rsLicitacion.getCodigo_tipo_licitacion());
				rsLicitacionCompleta.setFuente_financiamiento(rsLicitacion.getFuente_financiamiento());
				rsLicitacionCompleta.setMonto_estimado(rsLicitacion.getMonto_estimado());
				rsLicitacionCompleta.setNombre_responsable_contrato(rsLicitacion.getNombre_responsable_contrato());
				rsLicitacionCompleta.setEmail_responsable_contrato(rsLicitacion.getEmail_responsable_contrato());
				rsLicitacionCompleta.setNombre_responsable_pago(rsLicitacion.getNombre_responsable_pago());
				rsLicitacionCompleta.setFono_responsable_contrato(rsLicitacion.getFono_responsable_contrato());
				rsLicitacionCompleta.setEs_renovable(rsLicitacion.getEs_renovable());
				rsLicitacionCompleta.setCodigo_unidad(rsLicitacion.getCodigo_unidad());
				rsLicitacionCompleta.setCodigo_usuario(rsLicitacion.getCodigo_usuario());
				
				rsLicitacionCompleta.setFecha_creacion(rsLicitacion.getFecha_creacion());
				rsLicitacionCompleta.setFecha_cierre(rsLicitacion.getFecha_cierre());
				rsLicitacionCompleta.setFecha_acto_apertura_economica(rsLicitacion.getFecha_acto_apertura_economica());
				rsLicitacionCompleta.setFecha_apertura_tecnica(rsLicitacion.getFecha_apertura_tecnica());
				rsLicitacionCompleta.setFecha_publicacion(rsLicitacion.getFecha_publicacion());
				rsLicitacionCompleta.setFecha_adjudicacion(rsLicitacion.getFecha_adjudicacion());
				rsLicitacionCompleta.setFecha_estimada_adjudicacion(rsLicitacion.getFecha_estimada_adjudicacion());			
				
				// Buscamos los Items asociados a la Licitacion
				Util.loggerInfo(idEvento, "Obteniendo Datos Item... ");
				licitacionItems = bidMercadoPublicoDao.getItemsByIdLicitacion(rsLicitacion.getCodigo_licitacion(),idEvento);
				
				if(Util.isNotNull(licitacionItems)){
					Util.loggerInfo(idEvento, "Obteniendo Datos Item...OK ");
					rsListaItems = new ArrayList<BidLicitacionItem>();
					rsLicitacionItems = new BidLicitacionItems();
					
					for (BidLicitacionItem item : licitacionItems) {
						Util.loggerInfo(idEvento, "Obtiene Datos Licitacion:[" + id + "] | Item  [" + item.getCodigo_producto_item() + "]");
						rsLicitacionItem = new BidLicitacionItem();
						parameterOferta = new BidLicitacionOfertaRequest();
						rsLicitacionItem.setCorrelativo_item(item.getCorrelativo_item());
						rsLicitacionItem.setCodigo_licitacion(item.getCodigo_licitacion());
						rsLicitacionItem.setCantidad_item(item.getCantidad_item());
						rsLicitacionItem.setCodigo_producto_item(item.getCodigo_producto_item());
						rsLicitacionItem.setDescripcion_item(item.getDescripcion_item());
						
						rsLicitacionItem.setNombre_producto(item.getNombre_producto());
						rsLicitacionItem.setUnidad_producto(item.getUnidad_producto());
						rsLicitacionItem.setCodigo_categoria(item.getCodigo_categoria());
						rsLicitacionItem.setNombre_categoria(item.getNombre_categoria());
						
							parameterOferta.setCodigo_licitacion(item.getCodigo_licitacion());
							parameterOferta.setCorrelativo_item(item.getCorrelativo_item());
							Util.loggerInfo(idEvento, "Obteniendo Datos Oferta...");
							licitacionOfertas = bidMercadoPublicoDao.getOfertaByIdLicitacion(parameterOferta,idEvento);
							if(Util.isNotNull(licitacionOfertas)){
								Util.loggerInfo(idEvento, "Obtiene Datos Oferta... OK");
								rsListaOfertas = new ArrayList<BidLicitacionOferta>();
								rsLicitacionOfertas = new BidLicitacionOfertas();
								for (BidLicitacionOferta oferta : licitacionOfertas) {
									rsLicitacionOferta = new BidLicitacionOferta();
									Util.loggerInfo(idEvento, "Obtiene Datos Licitacion:[" + id + "] | Item  [" + item.getCorrelativo_item() + "] | Oferta [" + oferta.getCorrelativo_item_oferta() + "]" );
									
									rsLicitacionOferta.setCodigo_licitacion(oferta.getCodigo_licitacion());
									rsLicitacionOferta.setCantidad_oferta(oferta.getCantidad_oferta());
									rsLicitacionOferta.setCorrelativo_item_oferta(oferta.getCorrelativo_item_oferta());
									rsLicitacionOferta.setMonto_unitario_oferta(oferta.getMonto_unitario_oferta());
									rsLicitacionOferta.setRut_proveedor_oferta(oferta.getRut_proveedor_oferta());
									rsLicitacionOferta.setAdjudicada(oferta.getAdjudicada());
									rsLicitacionOferta.setEspecificaciones(oferta.getEspecificaciones());
									
									rsListaOfertas.add(rsLicitacionOferta);
								}
								rsLicitacionOfertas.setOfertasList(rsListaOfertas);
								rsLicitacionItem.setItemOferta(rsLicitacionOfertas);
							}else {
								Util.loggerInfo(idEvento, "No existen Ofertas para Licitacion: [" + id + "] | Item  [" + item.getCorrelativo_item() + "]" );
							}
						rsListaItems.add(rsLicitacionItem);
					}
					rsLicitacionItems.setItemsList(rsListaItems);
					rsLicitacionCompleta.setLicitacionItems(rsLicitacionItems);
				}else {
					Util.loggerInfo(idEvento, "No existen Items para la licitacion: "+id);
				}
			
				Util.loggerInfo(idEvento, "Obteniendo Datos Criterios... ");
				licitacionCriterios = bidMercadoPublicoDao.getCriterioByIdLicitacion(id,idEvento);
				if(Util.isNotNull(licitacionCriterios)) {
					Util.loggerInfo(idEvento, "Obteniendo Datos Criterios...OK ");
					rsListaCriterios = new ArrayList<BidLicitacionCriterio>();
					rsLictacionCriterios = new BidLicitacionCriterios();
					for(BidLicitacionCriterio criterio : licitacionCriterios) {
						rsLicitacionCriterio = new BidLicitacionCriterio();
						Util.loggerInfo(idEvento, " getCriterioByIdLicitacion Correlativo: "+criterio.getCorrelativo_criterio());
						rsLicitacionCriterio.setCodigo_licitacion(criterio.getCodigo_licitacion());
						rsLicitacionCriterio.setCorrelativo_criterio(criterio.getCorrelativo_criterio());
						rsLicitacionCriterio.setDescripcion_criterio(criterio.getDescripcion_criterio());
						rsLicitacionCriterio.setItems_criterio(criterio.getItems_criterio());
						rsLicitacionCriterio.setPonderacion_criterio(criterio.getPonderacion_criterio());
						rsListaCriterios.add(rsLicitacionCriterio);
					}
					rsLictacionCriterios.setCriteriosList(rsListaCriterios);
					rsLicitacionCompleta.setLicitacionCriterios(rsLictacionCriterios);
				} else {
					Util.loggerInfo(idEvento, "No existen Datos de Criterios para la Licitaicion "+id);
				}		
			
				Util.loggerInfo(idEvento, "Obteniendo Datos Garantia... ");
				licitacionGarantias = bidMercadoPublicoDao.getGarantiaByIdLicitacion(id,idEvento);
				if(Util.isNotNull(licitacionGarantias)) {
					Util.loggerInfo(idEvento, "Obteniendo Datos Garantia...OK ");
					rsListaGarantias = new ArrayList<BidLicitacionGarantia>();
					rsLicitacionGarantias = new BidLicitacionGarantias();
					for (BidLicitacionGarantia garantia : licitacionGarantias) {
						rsLicitacionGarantia = new  BidLicitacionGarantia();
	
						Util.loggerInfo(idEvento, " getGarantiaByIdLicitacion Correlativo: "+garantia.getCorrelativo_garantia());
						rsLicitacionGarantia.setCodigo_licitacion(garantia.getCodigo_licitacion());
						rsLicitacionGarantia.setCorrelativo_garantia(garantia.getCorrelativo_garantia());
						rsLicitacionGarantia.setBeneficiario_garantia(garantia.getBeneficiario_garantia());
						rsLicitacionGarantia.setDescripcion_garantia(garantia.getDescripcion_garantia());
						rsLicitacionGarantia.setFecha_vencimiento_garantia(garantia.getFecha_vencimiento_garantia());
						rsLicitacionGarantia.setGlosa_garantia(garantia.getGlosa_garantia());
						rsLicitacionGarantia.setMoneda_garantia(garantia.getMoneda_garantia());
						rsLicitacionGarantia.setRestitucion_garantia(garantia.getRestitucion_garantia());
						rsListaGarantias.add(rsLicitacionGarantia);
					}
					rsLicitacionGarantias.setGarantiaList(rsListaGarantias);
					rsLicitacionCompleta.setLicitacionGarantias(rsLicitacionGarantias);				
				}else {
					Util.loggerInfo(idEvento, "No existen Datos de Garantia para la licitacion "+id);
				}
				
				/*********************** MONTOS DETALLE *******************************/
				
				Util.loggerInfo(idEvento, "Obteniendo Datos Montos Licitacion... ");
				licitacionDetalleMontos = bidMercadoPublicoDao.getLicitacionDetalleMontoById(id, idEvento);
				if(Util.isNotNull(licitacionDetalleMontos)) {
					Util.loggerInfo(idEvento, "Obteniendo Montos Detalle...OK ");
					rsListaMontos = new ArrayList<BidLicitacionDetalleMonto>();
					rsLicitacionMontos = new BidLicitacionDetalleMontos();
					for (BidLicitacionDetalleMonto detalleMonto : licitacionDetalleMontos) {
						rsLicitacionMonto = new  BidLicitacionDetalleMonto();

						rsLicitacionMonto.setCodigo_licitacion(id);
						//rsLicitacionMonto.setMonto_estimado_contrato(Util.convertFormatCurrency(Util.convetString2double(detalleMonto.getMonto_estimado_contrato())));
						rsLicitacionMonto.setMonto_estimado_contrato(detalleMonto.getMonto_estimado_contrato());
						//rsLicitacionMonto.setMonto_neto_adjudicado(Util.convertFormatCurrency(Util.convetString2double(detalleMonto.getMonto_neto_adjudicado())));
						rsLicitacionMonto.setMonto_neto_adjudicado(detalleMonto.getMonto_neto_adjudicado());
						rsListaMontos.add(rsLicitacionMonto);
					}
					rsLicitacionMontos.setMontosList(rsListaMontos);
					
					rsLicitacionCompleta.setLicitacionMontos(rsLicitacionMontos);
				}
				
				
				/******************** ORGANISMO *****************************/
				
				Util.loggerInfo(idEvento, "Obteniendo Datos Organismo de Licitacion ");
				licitacionUnidadesDeCompra = bidMercadoPublicoDao.getLicitacionUnidadDeCompraById(id, idEvento);
				if(Util.isNotNull(licitacionUnidadesDeCompra)) {
					Util.loggerInfo(idEvento, "Obteniendo Datos Organismo de Licitacion ...  OK");
					rsListaUnidadesDeCompras = new ArrayList<BidLicitacionUnidadDeCompra>();
					rsLicitacionUnidadesDeCompras = new BidLicitacionUnidadDeCompras();
					
					for (BidLicitacionUnidadDeCompra unidadDeCompra : licitacionUnidadesDeCompra) {
						rsLicitacionUnidadDeCompra = new BidLicitacionUnidadDeCompra();
						
						rsLicitacionUnidadDeCompra.setCodigo_unidad(unidadDeCompra.getCodigo_unidad());
						rsLicitacionUnidadDeCompra.setCodigo_organismo(unidadDeCompra.getCodigo_organismo());
						rsLicitacionUnidadDeCompra.setRut_unidad(unidadDeCompra.getRut_unidad());
						rsLicitacionUnidadDeCompra.setRazon_social_unidad(unidadDeCompra.getRazon_social_unidad());
						rsLicitacionUnidadDeCompra.setNombre_unidad(unidadDeCompra.getNombre_unidad());
						rsLicitacionUnidadDeCompra.setRegion_unidad(unidadDeCompra.getRegion_unidad());
						rsLicitacionUnidadDeCompra.setComuna_unidad(unidadDeCompra.getComuna_unidad());
						rsLicitacionUnidadDeCompra.setDireccion_unidad(unidadDeCompra.getDireccion_unidad());
						
						rsLicitacionUnidadDeCompra.setNombre_organismo(unidadDeCompra.getNombre_organismo());
						rsLicitacionUnidadDeCompra.setNombre_organismo(unidadDeCompra.getNombre_organismo());
						rsListaUnidadesDeCompras.add(rsLicitacionUnidadDeCompra);
					}
					rsLicitacionUnidadesDeCompras.setUnidadDeComprasList(rsListaUnidadesDeCompras);

					rsLicitacionCompleta.setLicitacionUnidadCompras(rsLicitacionUnidadesDeCompras);
				}
				
				licitacionResponse.setBidLicitacionCompleta(rsLicitacionCompleta);
				licitacionResponse.setCodigo_error(Util.COD_OK);
				licitacionResponse.setMensaje_error(Util.ESTADO_OK);
			}else {
				licitacionResponse.setCodigo_error(Util.COD_ERROR);
				licitacionResponse.setMensaje_error(Util.ESTADO_WARNING +" | "+Util.MSG_WARNING_LICITACION_NO_EXISTE);
			}
		
		} catch (Exception e) {
			licitacionResponse.setCodigo_error(Util.COD_ERROR);
			licitacionResponse.setMensaje_error(Util.ESTADO_ERROR+ e);
		}
		Util.loggerDebug(idEvento, "Termino servicio api getLicitacionById");
		return licitacionResponse;
	}

	
	@Override
	public List<BidLicitacionBasica> getLicitacionByIdEstado(int cod_estado) throws BidServiceException {
		long idEvento = Util.obtenerIdEvento();

		Util.loggerDebug(idEvento, "Inicio servicio api getLicitacionByIdEstado");
		List<BidLicitacionBasica> listLicitaciones;
		
		try {
			listLicitaciones = bidMercadoPublicoDao.getLicitacionByIdEstado(cod_estado,idEvento); 
		} catch (Exception e) {
			throw (e);
		}

		Util.loggerDebug(idEvento, "Termino servicio api getLicitacionByIdEstado");
		return listLicitaciones;
	}
	
	@Override
	public BidProveedoresResponse postProveedoresByRut(List<BidProveedorRequest> parameters) throws BidServiceException {
		
		long idEvento = Util.obtenerIdEvento();
		BidProveedoresResponse proveedoresResponse = new BidProveedoresResponse();
		BidProveedores proveedores;
		List<BidProveedor> rsProveedores;
		BidProveedor bidProveedor;
		
		try {
			if(Util.isNotNull(parameters)) {
			
				rsProveedores = new ArrayList<BidProveedor>();
				for (BidProveedorRequest proveedor : parameters) {
					Util.loggerInfo(idEvento,"get rut Proveedor [" + proveedor.getRut_proveedor() + "]");
					bidProveedor = bidMercadoPublicoDao.getProveedorByRut(idEvento, proveedor.getRut_proveedor());
					rsProveedores.add(bidProveedor);
				}
				proveedores = new BidProveedores();
				proveedores.setBidProveedoresList(rsProveedores);
				proveedoresResponse.setBidproveedores(proveedores);
				
				proveedoresResponse.setCodigo_error(0);
				proveedoresResponse.setMensaje_error(Util.ESTADO_OK);
			
			} else {
				proveedoresResponse.setCodigo_error(0);
				proveedoresResponse.setMensaje_error(Util.ESTADO_WARNING +"-"+ Util.MSG_FILTROS_BUSQUEDA_INCOMPLETOS);	
			}
		} catch (Exception e) {
			proveedoresResponse.setCodigo_error(-1);
			proveedoresResponse.setMensaje_error(Util.ESTADO_ERROR+ e);
		}
		return proveedoresResponse;
	}
	
	@Override
	public BidProveedorResponse postProveedorByRut(BidProveedorRequest parameters) throws BidServiceException,DataAccessException {
		
		BidProveedorResponse proveedorResponse = new BidProveedorResponse();
		long idEvento = Util.obtenerIdEvento();
		BidProveedor rsProveedor;
		try {
			
			if(Util.isNotNull(parameters)) {
				rsProveedor = bidMercadoPublicoDao.postProveedor(idEvento, parameters);
				if(Util.isNotNull(rsProveedor)) {
					proveedorResponse.setBidProveedor(rsProveedor);
					proveedorResponse.setCodigo_error(0);
					proveedorResponse.setMensaje_error(Util.ESTADO_OK);
				} else {
					proveedorResponse.setCodigo_error(-1);
					proveedorResponse.setMensaje_error(Util.ESTADO_WARNING +"|"+Util.MSG_WARNING_PROVEEDOR_NO_EXISTE);					
				}
			}else {
				proveedorResponse.setCodigo_error(-1);
				proveedorResponse.setMensaje_error(Util.ESTADO_ERROR +"-"+Util.MSG_FILTROS_BUSQUEDA_INCOMPLETOS);			
			}
		} catch (DataAccessException ex) {
			proveedorResponse.setCodigo_error(-1);
			proveedorResponse.setMensaje_error(Util.ESTADO_ERROR+ ex);			
		} catch (Exception e) {
			proveedorResponse.setCodigo_error(-1);
			proveedorResponse.setMensaje_error(Util.ESTADO_ERROR+ e);
		}
		return proveedorResponse;
	}
	
	@Override
	public BidProveedorResponse getProveedorByRut(String rut) throws BidServiceException,DataAccessException {
		
		BidProveedorResponse proveedorResponse = new BidProveedorResponse();
		long idEvento = Util.obtenerIdEvento();
		BidProveedor rsProveedor;
		Util.loggerInfo(idEvento, "Inicio servicio api getProveedorByRut");
		try {
			
			if(Util.isNotNull(rut)) {
				Util.loggerInfo(idEvento, "Obteniendo Datos Proveedor....");
				rsProveedor = bidMercadoPublicoDao.getProveedorByRut(idEvento, rut);
				//if(Util.isNotNull(rsProveedor)&&!rsProveedor.getRut_proveedor().equals("-1")) {
				if(Util.isNotNull(rsProveedor)&&(!rsProveedor.getCodigo_error().equals(Util.COD_ERROR))) {
					Util.loggerInfo(idEvento, "Obteniendo Datos Proveedor...OK");
					proveedorResponse.setBidProveedor(rsProveedor);
					proveedorResponse.setCodigo_error(Util.COD_OK);
					proveedorResponse.setMensaje_error(Util.ESTADO_OK);
				} else {
					proveedorResponse.setCodigo_error(Util.COD_ERROR);
					proveedorResponse.setMensaje_error(Util.ESTADO_WARNING +"|"+Util.MSG_WARNING_PROVEEDOR_NO_EXISTE);	
				}
			}else {
				proveedorResponse.setCodigo_error(Util.COD_ERROR);
				proveedorResponse.setMensaje_error(Util.ESTADO_ERROR +"-"+Util.MSG_FILTROS_BUSQUEDA_INCOMPLETOS);			
			}
		} catch (DataAccessException ex) {
			proveedorResponse.setCodigo_error(Util.COD_ERROR);
			proveedorResponse.setMensaje_error(Util.ESTADO_ERROR+ ex);			
		} catch (Exception e) {
			proveedorResponse.setCodigo_error(Util.COD_ERROR);
			proveedorResponse.setMensaje_error(Util.ESTADO_ERROR+ e);
		}
		Util.loggerInfo(idEvento, "Termino servicio api getProveedorByRut");
		return proveedorResponse;
	}
	
	@Override
	public BidItemsResponse getItemsByIdLicitacion(String id) throws BidServiceException {
		long idEvento = Util.obtenerIdEvento();
		Util.loggerInfo(idEvento, "get Item by IdLicitacion [" + id + "]");
				
		BidItemsResponse itemsResponse = new BidItemsResponse();
		List<BidLicitacionItem> itemsLicitacion = new ArrayList<BidLicitacionItem>();
		try {
			if(Util.isNotNullString(id)) {
				itemsLicitacion = bidMercadoPublicoDao.getItemsByIdLicitacion(id,idEvento);
				itemsResponse.setItemsList(itemsLicitacion);
				itemsResponse.setMensaje_error(Util.ESTADO_OK);
			}else {
				itemsResponse.setItemsList(itemsLicitacion);
				itemsResponse.setMensaje_error(Util.ESTADO_WARNING +"-"+ Util.MSG_FILTROS_BUSQUEDA_INCOMPLETOS);				
			}
			
		} catch (Exception e) {
			itemsResponse.setCodigo_error(-1);
			itemsResponse.setMensaje_error(Util.ESTADO_ERROR + e);
		}
		return itemsResponse;
		
	}
	
	@Override
	public BidLicitacionesBasicasResponse getLicitacionesByFechaAdjudicada(final Date fecha) throws BidServiceException, DataAccessException {
		long idEvento = Util.obtenerIdEvento();
		BidLicitacionesBasicasResponse BidlicitacionesBasicasResponse = new BidLicitacionesBasicasResponse();
		List<BidLicitacionBasica> licitacionesList;
		List<BidLicitacionBasica> rsLicitacionBasicaList;
		BidLicitacionBasica rsLicitacionBasica;
		BidLicitacionesBasicas rsLicitacionesBasicas;
		int aux=0;
				
		try {
			Util.loggerInfo(idEvento, "Inicio get Licitacion by fecha adjudicada"+fecha);
			Date fecha1 = Util.fechaIni(fecha);
			Date fecha2 = Util.fechaFin(fecha);
			
			if(Util.isNotNull(fecha1)) {
				rsLicitacionBasicaList = new ArrayList<BidLicitacionBasica>();
				Util.loggerInfo(idEvento, "Obteniendo Licitacion con fecha adjudicada....");
				licitacionesList = bidMercadoPublicoDao.getLicitacionesByFechaAdjudicada(fecha1,fecha2,idEvento);
				if(Util.isNotNull(licitacionesList)) {			
				rsLicitacionesBasicas = new BidLicitacionesBasicas();
				Util.loggerInfo(idEvento, "Obteniendo Licitacion con fecha adjudicada....OK");
				for (BidLicitacionBasica licitacion : licitacionesList) {
					rsLicitacionBasica = new BidLicitacionBasica();
					rsLicitacionBasica.setCodigo_licitacion(licitacion.getCodigo_licitacion());
					rsLicitacionBasica.setNombre_licitacion(licitacion.getNombre_licitacion());
					rsLicitacionBasica.setCodigo_estado_licitacion(licitacion.getCodigo_estado_licitacion());
					rsLicitacionBasica.setFecha_adjudicacion(licitacion.getFecha_adjudicacion());
					rsLicitacionBasicaList.add(rsLicitacionBasica);
					aux=aux+1;
				}
				rsLicitacionesBasicas.setLicitacionesBasicasList(rsLicitacionBasicaList);
				BidlicitacionesBasicasResponse.setLicitacionesBasicas(rsLicitacionesBasicas);
				BidlicitacionesBasicasResponse.setCodigo_error(0);
				BidlicitacionesBasicasResponse.setMensaje_error(Util.ESTADO_OK + " | Listado de: "+aux+ " Licitaciones");
				} else {
					BidlicitacionesBasicasResponse.setCodigo_error(0);
					BidlicitacionesBasicasResponse.setMensaje_error(Util.ESTADO_WARNING + " | "+ Util.MSG_WARNING_LICITACIONES_NO_EXISTE);					
				}
			}else {
				BidlicitacionesBasicasResponse.setCodigo_error(0);
				BidlicitacionesBasicasResponse.setMensaje_error(Util.ESTADO_WARNING + " | "+ Util.MSG_FILTROS_BUSQUEDA_INCOMPLETOS);					
			}
		}catch (DataAccessException ex){
			BidlicitacionesBasicasResponse.setCodigo_error(-1);
			BidlicitacionesBasicasResponse.setMensaje_error(Util.ESTADO_ERROR + ex);
		} catch (Exception e) {
			BidlicitacionesBasicasResponse.setCodigo_error(-1);
			BidlicitacionesBasicasResponse.setMensaje_error(Util.ESTADO_ERROR + e);
		}
		Util.loggerDebug(idEvento, "Fin get Licitacion by fecha adjudicada");
		return BidlicitacionesBasicasResponse;
	}
	
	
	
	
	
}