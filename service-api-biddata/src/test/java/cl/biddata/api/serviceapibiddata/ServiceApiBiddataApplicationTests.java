package cl.biddata.api.serviceapibiddata;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceApiBiddataApplicationTests {

	@Test
	public void contextLoads() {
	}

}
